package com.platform.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.platform.dao.ApiRecordItemMapper;
import com.platform.dao.ApiRecordMapper;
import com.platform.dao.ApiTodayRankMapper;
import com.platform.dao.ApiTotalRankMapper;
import com.platform.dao.ApiWeekRankMapper;
import com.platform.entity.RecordItemVo;
import com.platform.entity.RecordVo;
import com.platform.entity.TodayRankVo;
import com.platform.entity.TotalRankVo;
import com.platform.entity.UserVo;
import com.platform.entity.WeekRankVo;
import com.platform.utils.DateUtils;

/**
 * 打卡表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 * 小程序视频申请
 * https://segmentfault.com/a/1190000012007132
 * date format
 * https://blog.csdn.net/molashaonian/article/details/53025118
 * mybatis查询时间段sql语句和DATE_FORMAT
 * https://blog.csdn.net/w_wensan/article/details/76513085
 * mysql 查询今天、昨天、上月、本月的数据
 * https://blog.csdn.net/wangjuan_01/article/details/51726588
 */
@Service
public class ApiRecordService {
    @Autowired
    private ApiRecordMapper recordDao;
    @Autowired
    private ApiTotalRankMapper totalRankDao;
    @Autowired
    private ApiTodayRankMapper todayRankDao;
    @Autowired
    private ApiWeekRankMapper weekRankDao;
    @Autowired
    private ApiRecordItemMapper recordItemDao;

    @Transactional
    public Map<String,Object> recordAdd(RecordVo record,UserVo loginUser){
    	Map<String,Object> resultObj = new HashMap<String,Object>();
    	List<RecordItemVo> itemList = new ArrayList<RecordItemVo>();
    	record.setUserId(loginUser.getUserId());
//    	record.setUserName(loginUser.getNickname());
    	record.setUserName(loginUser.getRealname());
//    	if(record.getMediaUrl().contains(".mp4")) {
//    		record.setMediaTypeId(20);
//        	record.setMediaTypeText("video");
//    	}else {
//    		record.setMediaTypeId(10);
//        	record.setMediaTypeText("image");
//    	}
    	Date nowTime = new Date();
    	record.setCreateTime(nowTime);
    	record.setRecordTime(nowTime);
    	record.setStatusId(1);
    	
//    	recordDao.save(record);
    	record = save(record);
    	if (null == record.getId()) {
            resultObj.put("errno", 1);
            resultObj.put("errmsg", "打卡提交失败");
            return resultObj;
        }
    	RecordItemVo recordItem = new RecordItemVo();
    	if(record.getRecordTypeId()==20) {//阻尼
    		itemList = record.getItemList();
    		Date createTime = new Date();
    		for(int i=0;i<itemList.size();i++) {
    			recordItem = itemList.get(i);
    			recordItem.setRecordId(record.getId());
    			recordItem.setCreateTime(createTime);
    			recordItemDao.save(recordItem);
        	}
    	}
    	TodayRankVo todayRankVo = new TodayRankVo();
    	todayRankVo.setUserId(record.getUserId());
    	todayRankVo = todayRankDao.queryMaxIdObject(todayRankVo);
    	if(todayRankVo==null || todayRankVo.getId()==null) {
    		//新增
    		todayRankVo = new TodayRankVo();
    		todayRankVo.setTodayCount(1);
        	todayRankVo.setTodayMinute(record.getRecordMinute());
        	todayRankVo.setTodayTime(nowTime);
        	todayRankVo.setUserId(record.getUserId());
        	todayRankVo.setUserName(record.getUserName());
        	todayRankDao.save(todayRankVo);
    	}else {
    		int todayCount = todayRankVo.getTodayCount() + 1;
    		todayRankVo.setTodayCount(todayCount);
    		int todayMinute = todayRankVo.getTodayMinute() + record.getRecordMinute();
    		todayRankVo.setTodayMinute(todayMinute);
    		todayRankDao.update(todayRankVo);
    	}
    	
    	
    	TotalRankVo totalRankVo = new TotalRankVo();
    	totalRankVo.setUserId(record.getUserId());
    	totalRankVo = totalRankDao.queryMaxIdObject(totalRankVo);
    	if(totalRankVo==null || totalRankVo.getId()==null) {
    		totalRankVo = new TotalRankVo();
    		totalRankVo.setUserId(record.getUserId());
        	totalRankVo.setUserName(record.getUserName());
        	totalRankVo.setTotalCount(1);
        	totalRankVo.setStartTime(nowTime);
        	totalRankVo.setEndTime(nowTime);
        	totalRankVo.setTotalMinute(record.getRecordMinute());
        	totalRankDao.save(totalRankVo);
    	}else {
    		int totalCount = totalRankVo.getTotalCount() + 1;
    		totalRankVo.setTotalCount(totalCount);
    		int totalMinute = totalRankVo.getTotalMinute() + record.getRecordMinute();
    		totalRankVo.setTotalMinute(totalMinute);
    		totalRankVo.setEndTime(nowTime);
    		totalRankDao.update(totalRankVo);
    	}
    	
    	
    	WeekRankVo weekRankVo = new WeekRankVo();
    	weekRankVo.setUserId(record.getUserId());
    	weekRankVo = weekRankDao.queryMaxIdObject(weekRankVo);
    	if(weekRankVo==null || weekRankVo.getId()==null) {
    		weekRankVo = new WeekRankVo();
    		weekRankVo.setEndTime(DateUtils.getPreviousSundayDate());
        	weekRankVo.setStartTime(DateUtils.getCurrentMondayDate());
        	weekRankVo.setUserId(record.getUserId());
        	weekRankVo.setUserName(record.getUserName());
        	weekRankVo.setWeekCount(1);
        	weekRankVo.setWeekMinute(record.getRecordMinute());
        	weekRankDao.save(weekRankVo);
    	}else {
    		int weekCount = totalRankVo.getTotalCount() + 1;
    		weekRankVo.setWeekCount(weekCount);
    		int weekMinute = weekRankVo.getWeekMinute() + record.getRecordMinute();
    		weekRankVo.setWeekMinute(weekMinute);
    		weekRankDao.update(weekRankVo);
    	}
    	
    	resultObj.put("errno", 0);
        resultObj.put("errmsg", "提交成功");
    	return resultObj;
    }
    
    
    public RecordVo queryObject(Long id) {
        return recordDao.queryObject(id);
    }

    
    public List<RecordVo> queryList(Map<String, Object> map) {
        return recordDao.queryList(map);
    }

    
    public int queryTotal(Map<String, Object> map) {
        return recordDao.queryTotal(map);
    }

    
    public RecordVo save(RecordVo record) {
         recordDao.save(record);
         return record;
    }

    
    public int update(RecordVo record) {
        return recordDao.update(record);
    }

    
    public int delete(Long id) {
        return recordDao.delete(id);
    }

    
    public int deleteBatch(Long[]ids) {
        return recordDao.deleteBatch(ids);
    }
}

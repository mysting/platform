package com.platform.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.ApiTotalRankMapper;
import com.platform.entity.TotalRankVo;

/**
 * 总排行表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:43
 */
@Service
public class ApiTotalRankService {
    @Autowired
    private ApiTotalRankMapper totalRankDao;

    
    public TotalRankVo queryObject(Integer id) {
        return totalRankDao.queryObject(id);
    }

    
    public List<TotalRankVo> queryList(Map<String, Object> map) {
        return totalRankDao.queryList(map);
    }

    
    public int queryTotal(Map<String, Object> map) {
        return totalRankDao.queryTotal(map);
    }

    
    public int save(TotalRankVo totalRank) {
        return totalRankDao.save(totalRank);
    }

    
    public int update(TotalRankVo totalRank) {
        return totalRankDao.update(totalRank);
    }

    
    public int delete(Integer id) {
        return totalRankDao.delete(id);
    }

    
    public int deleteBatch(Integer[]ids) {
        return totalRankDao.deleteBatch(ids);
    }
    
    public TotalRankVo queryMaxIdObject(TotalRankVo totalRankVo) {
    	return totalRankDao.queryMaxIdObject(totalRankVo);
    }
    
    public List<TotalRankVo> queryRankList(TotalRankVo totalRankVo){
    	return totalRankDao.queryRankList(totalRankVo);
    }
}

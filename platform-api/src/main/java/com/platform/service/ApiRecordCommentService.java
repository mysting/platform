package com.platform.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.ApiRecordCommentMapper;
import com.platform.entity.RecordCommentVo;

/**
 * 打卡评论表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@Service
public class ApiRecordCommentService {
    @Autowired
    private ApiRecordCommentMapper recordCommentDao;

    public RecordCommentVo queryObject(Integer id) {
        return recordCommentDao.queryObject(id);
    }

    public List<RecordCommentVo> queryList(Map<String, Object> map) {
        return recordCommentDao.queryList(map);
    }

    public int queryTotal(Map<String, Object> map) {
        return recordCommentDao.queryTotal(map);
    }

    public int save(RecordCommentVo recordComment) {
        return recordCommentDao.save(recordComment);
    }

    public int update(RecordCommentVo recordComment) {
        return recordCommentDao.update(recordComment);
    }

    public int delete(Long id) {
        return recordCommentDao.delete(id);
    }

    public int deleteBatch(Long[]ids) {
        return recordCommentDao.deleteBatch(ids);
    }
}

package com.platform.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.ApiTodayRankMapper;
import com.platform.entity.TodayRankVo;
import com.platform.entity.WeekRankVo;

/**
 * 今日排行表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@Service
public class ApiTodayRankService {
    @Autowired
    private ApiTodayRankMapper todayRankDao;
    
    public TodayRankVo queryObject(Integer id) {
        return todayRankDao.queryObject(id);
    }

    
    public List<TodayRankVo> queryList(Map<String, Object> map) {
        return todayRankDao.queryList(map);
    }

    
    public int queryTotal(Map<String, Object> map) {
        return todayRankDao.queryTotal(map);
    }

    
    public int save(TodayRankVo todayRank) {
        return todayRankDao.save(todayRank);
    }

    
    public int update(TodayRankVo todayRank) {
        return todayRankDao.update(todayRank);
    }

    
    public int delete(Integer id) {
        return todayRankDao.delete(id);
    }

    
    public int deleteBatch(Integer[]ids) {
        return todayRankDao.deleteBatch(ids);
    }
    
    public TodayRankVo queryMaxIdObject(TodayRankVo todayRankVo) {
    	return todayRankDao.queryMaxIdObject(todayRankVo);
    }
    
    public List<TodayRankVo> queryRankList(TodayRankVo todayRankVo){
    	return todayRankDao.queryRankList(todayRankVo);
    }
}

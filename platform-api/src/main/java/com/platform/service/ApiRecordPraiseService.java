package com.platform.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.ApiRecordPraiseMapper;
import com.platform.entity.RecordPraiseVo;
/**
 * 打卡点赞表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@Service
public class ApiRecordPraiseService {
    @Autowired
    private ApiRecordPraiseMapper recordPraiseDao;

    
    public RecordPraiseVo queryObject(Integer id) {
        return recordPraiseDao.queryObject(id);
    }

    
    public List<RecordPraiseVo> queryList(Map<String, Object> map) {
        return recordPraiseDao.queryList(map);
    }

    
    public int queryTotal(Map<String, Object> map) {
        return recordPraiseDao.queryTotal(map);
    }

    
    public int save(RecordPraiseVo recordPraise) {
        return recordPraiseDao.save(recordPraise);
    }

    
    public int update(RecordPraiseVo recordPraise) {
        return recordPraiseDao.update(recordPraise);
    }

    
    public int delete(Integer id) {
        return recordPraiseDao.delete(id);
    }

    
    public int deleteBatch(Integer[]ids) {
        return recordPraiseDao.deleteBatch(ids);
    }
}

package com.platform.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.platform.dao.ApiWeekRankMapper;
import com.platform.entity.TodayRankVo;
import com.platform.entity.WeekRankVo;

import java.util.List;
import java.util.Map;

/**
 * 本周排行表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@Service
public class ApiWeekRankService {
    @Autowired
    private ApiWeekRankMapper weekRankDao;

    public WeekRankVo queryObject(Integer id) {
        return weekRankDao.queryObject(id);
    }

    
    public List<WeekRankVo> queryList(Map<String, Object> map) {
        return weekRankDao.queryList(map);
    }

    
    public int queryTotal(Map<String, Object> map) {
        return weekRankDao.queryTotal(map);
    }

    
    public int save(WeekRankVo weekRank) {
        return weekRankDao.save(weekRank);
    }

    
    public int update(WeekRankVo weekRank) {
        return weekRankDao.update(weekRank);
    }

    
    public int delete(Integer id) {
        return weekRankDao.delete(id);
    }

    
    public int deleteBatch(Integer[]ids) {
        return weekRankDao.deleteBatch(ids);
    }
    
    public WeekRankVo queryMaxIdObject(WeekRankVo weekRankVo) {
    	return weekRankDao.queryMaxIdObject(weekRankVo);
    }
    
    public List<WeekRankVo> queryRankList(WeekRankVo weekRankVo){
    	return weekRankDao.queryRankList(weekRankVo);
    }
}

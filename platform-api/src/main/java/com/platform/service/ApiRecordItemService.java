package com.platform.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.ApiRecordItemMapper;
import com.platform.entity.RecordItemVo;

/**
 * 打卡子项表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:43
 */
@Service
public class ApiRecordItemService {
    @Autowired
    private ApiRecordItemMapper recordItemDao;

    
    public RecordItemVo queryObject(Integer id) {
        return recordItemDao.queryObject(id);
    }

    
    public List<RecordItemVo> queryList(Map<String, Object> map) {
        return recordItemDao.queryList(map);
    }

    
    public int queryTotal(Map<String, Object> map) {
        return recordItemDao.queryTotal(map);
    }

    
    public int save(RecordItemVo recordItem) {
        return recordItemDao.save(recordItem);
    }

    
    public int update(RecordItemVo recordItem) {
        return recordItemDao.update(recordItem);
    }

    
    public int delete(Integer id) {
        return recordItemDao.delete(id);
    }

    
    public int deleteBatch(Integer[]ids) {
        return recordItemDao.deleteBatch(ids);
    }
}

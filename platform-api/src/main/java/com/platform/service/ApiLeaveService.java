package com.platform.service;

import com.platform.dao.ApiAdMapper;
import com.platform.dao.ApiLeaveMapper;
import com.platform.entity.LeaveVo;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 会员请假表Service接口
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-08-19 22:17:31
 */
@Service
public class ApiLeaveService {
	
	@Autowired
    private ApiLeaveMapper leaveDao;

    /**
     * 根据主键查询实体
     *
     * @param id 主键
     * @return 实体
     */
    public LeaveVo queryObject(Long id){
        return leaveDao.queryObject(id);
    }

    /**
     * 分页查询
     *
     * @param map 参数
     * @return list
     */
    public List<LeaveVo> queryList(Map<String, Object> map){
    	return leaveDao.queryList(map);
    }

    /**
     * 分页统计总数
     *
     * @param map 参数
     * @return 总数
     */
    public int queryTotal(Map<String, Object> map) {
    	return leaveDao.queryTotal(map);
    }

    /**
     * 保存实体
     *
     * @param leave 实体
     * @return 保存条数
     */
    public int save(LeaveVo leave) {
    	return leaveDao.save(leave);
    }

    /**
     * 根据主键更新实体
     *
     * @param leave 实体
     * @return 更新条数
     */
    public int update(LeaveVo leave) {
    	return leaveDao.update(leave);
    }

    /**
     * 根据主键删除
     *
     * @param id
     * @return 删除条数
     */
    public int delete(Long id) {
    	return leaveDao.delete(id);
    }

    /**
     * 根据主键批量删除
     *
     * @param ids
     * @return 删除条数
     */
    public int deleteBatch(Long[]ids) {
    	return leaveDao.deleteBatch(ids);
    }
}

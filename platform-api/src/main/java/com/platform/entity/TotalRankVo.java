package com.platform.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * 总排行表实体
 * 表名 gaojin_total_rank
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:43
 */
public class TotalRankVo implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long id;
    //用户ID
    private Long userId;
    //用户姓名
    private String userName;
    //
    private Integer totalCount;
    //
    private Integer totalMinute;
    //开始时间
    private Date startTime;
    //结束时间
    private Date endTime;
    
    private Integer rownum;
    private String avatar;

    public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

    public Integer getRownum() {
		return rownum;
	}

	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}

    /**
     * 设置：
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置：用户ID
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取：用户ID
     */
    public Long getUserId() {
        return userId;
    }
    /**
     * 设置：用户姓名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取：用户姓名
     */
    public String getUserName() {
        return userName;
    }
    /**
     * 设置：
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * 获取：
     */
    public Integer getTotalCount() {
        return totalCount;
    }
    /**
     * 设置：
     */
    public void setTotalMinute(Integer totalMinute) {
        this.totalMinute = totalMinute;
    }

    /**
     * 获取：
     */
    public Integer getTotalMinute() {
        return totalMinute;
    }
    /**
     * 设置：开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取：开始时间
     */
    public Date getStartTime() {
        return startTime;
    }
    /**
     * 设置：结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取：结束时间
     */
    public Date getEndTime() {
        return endTime;
    }
}

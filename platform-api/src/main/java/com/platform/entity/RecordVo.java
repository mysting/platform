package com.platform.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * 打卡表实体
 * 表名 gaojin_record
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
public class RecordVo implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long id;
    //用户ID
    private Long userId;
    //用户姓名
    private String userName;
    //打卡类别
    private Integer recordTypeId;
    //打卡类别名称
    private String recordTypeText;
    //媒体类别
    private Integer mediaTypeId;
    //媒体类别名
    private String mediaTypeText;
    //打卡分钟
    private Integer recordMinute;
    //状态值1有效 0无效
    private Integer statusId;
    //状态值名
    private String statusText;
    //媒体url
    private String mediaUrl;
    //纬度
    private String latitude;
    //经度
    private String longitude;
    //地址
    private String address;
    //创建时间
    private Date createTime;
    //打卡时间
    private Date recordTime;
    //审核时间
    private Date confirmTime;
    
    private String action;
    private Double loadWeight;
    private Integer group;
    private Integer number;
    private String desc;
    
    private String avatar;
    List<RecordPraiseVo> recordPraiseList;
    List<RecordCommentVo> recordCommentList;
    List<RecordItemVo> itemList;
    
    public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
    public List<RecordPraiseVo> getRecordPraiseList() {
		return recordPraiseList;
	}

	public void setRecordPraiseList(List<RecordPraiseVo> recordPraiseList) {
		this.recordPraiseList = recordPraiseList;
	}

	public List<RecordCommentVo> getRecordCommentList() {
		return recordCommentList;
	}

	public void setRecordCommentList(List<RecordCommentVo> recordCommentList) {
		this.recordCommentList = recordCommentList;
	}

	public List<RecordItemVo> getItemList() {
		return itemList;
	}
	public void setItemList(List<RecordItemVo> itemList) {
		this.itemList = itemList;
	}
	/**
     * 设置：
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置：用户ID
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取：用户ID
     */
    public Long getUserId() {
        return userId;
    }
    /**
     * 设置：用户姓名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取：用户姓名
     */
    public String getUserName() {
        return userName;
    }
    /**
     * 设置：打卡类别
     */
    public void setRecordTypeId(Integer recordTypeId) {
        this.recordTypeId = recordTypeId;
    }

    /**
     * 获取：打卡类别
     */
    public Integer getRecordTypeId() {
        return recordTypeId;
    }
    /**
     * 设置：打卡类别名称
     */
    public void setRecordTypeText(String recordTypeText) {
        this.recordTypeText = recordTypeText;
    }

    /**
     * 获取：打卡类别名称
     */
    public String getRecordTypeText() {
    	if (null != mediaTypeId && StringUtils.isEmpty(recordTypeText)) {
    		recordTypeText = "有氧";
            switch (mediaTypeId) {
                case 10:
                	recordTypeText = "有氧";
                    break;
                case 20:
                	recordTypeText = "抗阻";
                    break;
                case 30:
                	recordTypeText = "羽毛球";
                    break;
                case 40:
                	recordTypeText = "高尔夫球";
                    break;
                case 50:
                	recordTypeText = "游泳";
                    break;
                case 60:
                	recordTypeText = "舞蹈";
                    break;
                case 70:
                	recordTypeText = "其他";
                    break;
            }
        }
        return recordTypeText;
    }
    /**
     * 设置：媒体类别
     */
    public void setMediaTypeId(Integer mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    /**
     * 获取：媒体类别
     */
    public Integer getMediaTypeId() {
        return mediaTypeId;
    }
    /**
     * 设置：媒体类别名
     */
    public void setMediaTypeText(String mediaTypeText) {
        this.mediaTypeText = mediaTypeText;
    }

    /**
     * 获取：媒体类别名
     */
    public String getMediaTypeText() {
    	if (null != mediaTypeId && StringUtils.isEmpty(mediaTypeText)) {
    		mediaTypeText = "image";
            switch (mediaTypeId) {
                case 10:
                	mediaTypeText = "image";
                    break;
                case 20:
                	mediaTypeText = "video";
                    break;
            }
        }
        return mediaTypeText;
    }
    /**
     * 设置：打卡分钟
     */
    public void setRecordMinute(Integer recordMinute) {
        this.recordMinute = recordMinute;
    }

    /**
     * 获取：打卡分钟
     */
    public Integer getRecordMinute() {
        return recordMinute;
    }
    /**
     * 设置：状态值1有效 0无效
     */
    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    /**
     * 获取：状态值1有效 0无效
     */
    public Integer getStatusId() {
        return statusId;
    }
    /**
     * 设置：状态值名
     */
    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    /**
     * 获取：状态值名
     */
    public String getStatusText() {
    	if (null != statusId && StringUtils.isEmpty(statusText)) {
    		statusText = "有效";
            switch (statusId) {
                case 0:
                	statusText = "无效";
                    break;
                case 1:
                	statusText = "有效";
                    break;
            }
        }
        return statusText;
    }
    /**
     * 设置：媒体url
     */
    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    /**
     * 获取：媒体url
     */
    public String getMediaUrl() {
        return mediaUrl;
    }
    /**
     * 设置：纬度
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * 获取：纬度
     */
    public String getLatitude() {
        return latitude;
    }
    /**
     * 设置：经度
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * 获取：经度
     */
    public String getLongitude() {
        return longitude;
    }
    /**
     * 设置：地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 获取：地址
     */
    public String getAddress() {
        return address;
    }
    /**
     * 设置：创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取：创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getCreateTime() {
        return createTime;
    }
    /**
     * 设置：打卡时间
     */
    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    /**
     * 获取：打卡时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getRecordTime() {
        return recordTime;
    }
    /**
     * 设置：审核时间
     */
    public void setConfirmTime(Date confirmTime) {
        this.confirmTime = confirmTime;
    }

    /**
     * 获取：审核时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getConfirmTime() {
        return confirmTime;
    }
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Double getLoadWeight() {
		return loadWeight;
	}
	public void setLoadWeight(Double loadWeight) {
		this.loadWeight = loadWeight;
	}
	public Integer getGroup() {
		return group;
	}
	public void setGroup(Integer group) {
		this.group = group;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
}

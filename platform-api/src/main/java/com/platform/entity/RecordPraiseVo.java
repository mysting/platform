package com.platform.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * 打卡点赞表实体
 * 表名 gaojin_record_praise
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
public class RecordPraiseVo implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long id;
    //
    private Long recordId;
    //用户ID
    private Long userId;
    //用户姓名
    private String userName;
    //状态值1有效 0无效
    private Integer statusId;
    //状态值名
    private String statusText;
    //点赞时间
    private Date praiseTime;

    /**
     * 设置：
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置：
     */
    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    /**
     * 获取：
     */
    public Long getRecordId() {
        return recordId;
    }
    /**
     * 设置：用户ID
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取：用户ID
     */
    public Long getUserId() {
        return userId;
    }
    /**
     * 设置：用户姓名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取：用户姓名
     */
    public String getUserName() {
        return userName;
    }
    /**
     * 设置：状态值1有效 0无效
     */
    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    /**
     * 获取：状态值1有效 0无效
     */
    public Integer getStatusId() {
        return statusId;
    }
    /**
     * 设置：状态值名
     */
    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    /**
     * 获取：状态值名
     */
    public String getStatusText() {
        return statusText;
    }
    /**
     * 设置：点赞时间
     */
    public void setPraiseTime(Date praiseTime) {
        this.praiseTime = praiseTime;
    }

    /**
     * 获取：点赞时间
     */
    public Date getPraiseTime() {
        return praiseTime;
    }
}

package com.platform.dao;

import java.util.List;

import com.platform.entity.TodayRankVo;
import com.platform.entity.WeekRankVo;

/**
 * 今日排行表Dao
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
public interface ApiTodayRankMapper extends BaseDao<TodayRankVo> {

	TodayRankVo queryMaxIdObject(TodayRankVo todayRankVo);
	
	List<TodayRankVo> queryRankList(TodayRankVo todayRankVo);
}

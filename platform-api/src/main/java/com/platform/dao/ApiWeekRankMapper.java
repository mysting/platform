package com.platform.dao;

import java.util.List;

import com.platform.entity.TotalRankVo;
import com.platform.entity.WeekRankVo;

/**
 * 本周排行表Dao
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
public interface ApiWeekRankMapper extends BaseDao<WeekRankVo> {

	WeekRankVo queryMaxIdObject(WeekRankVo weekRankVo);
	
	List<WeekRankVo> queryRankList(WeekRankVo weekRankVo);
}

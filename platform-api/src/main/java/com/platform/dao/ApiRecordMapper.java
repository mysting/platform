package com.platform.dao;

import com.platform.entity.RecordVo;

/**
 * 打卡表Dao
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
public interface ApiRecordMapper extends BaseDao<RecordVo> {

}

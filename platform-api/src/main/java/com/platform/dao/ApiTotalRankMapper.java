package com.platform.dao;

import java.util.List;

import com.platform.entity.TotalRankVo;

/**
 * 总排行表Dao
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:43
 */
public interface ApiTotalRankMapper extends BaseDao<TotalRankVo> {
	
	TotalRankVo queryMaxIdObject(TotalRankVo totalRankVo);
	
	List<TotalRankVo> queryRankList(TotalRankVo totalRankVo);
}

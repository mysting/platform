package com.platform.dao;

import com.platform.entity.RecordItemVo;

/**
 * 打卡子项表Dao
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:43
 */
public interface ApiRecordItemMapper extends BaseDao<RecordItemVo> {

}

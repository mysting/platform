package com.platform.api;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.LeaveVo;
import com.platform.service.ApiLeaveService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * 会员请假表Controller
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-08-19 22:17:31
 */
@RestController
@RequestMapping("api/leave")
public class ApiLeaveController {
    @Autowired
    private ApiLeaveService leaveService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("leave:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<LeaveVo> leaveList = leaveService.queryList(query);
        int total = leaveService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(leaveList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("leave:info")
    public R info(@PathVariable("id") Long id) {
        LeaveVo leave = leaveService.queryObject(id);

        return R.ok().put("leave", leave);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("leave:save")
    public R save(@RequestBody LeaveVo leave) {
        leaveService.save(leave);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("leave:update")
    public R update(@RequestBody LeaveVo leave) {
        leaveService.update(leave);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("leave:delete")
    public R delete(@RequestBody Long[]ids) {
        leaveService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<LeaveVo> list = leaveService.queryList(params);

        return R.ok().put("list", list);
    }
}

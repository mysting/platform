package com.platform.api;

import com.platform.annotation.IgnoreAuth;
import com.platform.entity.SysOssVo;
import com.platform.oss.OSSFactory;
import com.platform.util.ApiBaseAction;
import com.platform.utils.RRException;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 作者: @author Harmon <br>
 * 时间: 2017-09-08 13:20<br>
 * 描述: ApiUploadController <br>
 * 视频上传
 * https://www.cnblogs.com/smart-hwt/p/8256836.html
 * https://blog.csdn.net/baidu_35468322/article/details/54891289
 * https://www.jianshu.com/p/6f50130b6950
 * Yii
 * http://www.fancyecommerce.com/
 */
@RestController
@RequestMapping("/api/upload")
public class ApiUploadController extends ApiBaseAction {

    /**
     * 上传文件
     */
    @IgnoreAuth
    @RequestMapping("/upload")
    public Object upload(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }
        //上传文件
        String url = OSSFactory.build().upload(file);
        //保存文件信息
//        SysOssVo ossEntity = new SysOssVo();
//        ossEntity.setUrl(url);
//        ossEntity.setCreateDate(new Date());
//        sysOssService.save(ossEntity);
        
        return toResponsSuccess(url);
    }
}
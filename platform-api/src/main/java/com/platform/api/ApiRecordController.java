package com.platform.api;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.platform.annotation.IgnoreAuth;
import com.platform.annotation.LoginUser;
import com.platform.entity.AdVo;
import com.platform.entity.LeaveVo;
import com.platform.entity.RecordCommentVo;
import com.platform.entity.RecordItemVo;
import com.platform.entity.RecordPraiseVo;
import com.platform.entity.RecordVo;
import com.platform.entity.TodayRankVo;
import com.platform.entity.TotalRankVo;
import com.platform.entity.UserVo;
import com.platform.entity.WeekRankVo;
import com.platform.service.ApiAdService;
import com.platform.service.ApiLeaveService;
import com.platform.service.ApiRecordCommentService;
import com.platform.service.ApiRecordPraiseService;
import com.platform.service.ApiRecordService;
import com.platform.service.ApiTodayRankService;
import com.platform.service.ApiTotalRankService;
import com.platform.service.ApiUserService;
import com.platform.service.ApiWeekRankService;
import com.platform.util.ApiBaseAction;
import com.platform.util.ApiPageUtils;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * 打卡表Controller
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 * 在微信小程序中使用“随机键盘”
 * https://www.jianshu.com/p/06565eed9df3
 * https://github.com/JackieZheng/xxks.Jackie.wApp
 */
@RestController
@RequestMapping("/api/record")
public class ApiRecordController extends ApiBaseAction  {
	@Autowired
	private ApiLeaveService leaveService;
    @Autowired
    private ApiRecordService recordService;
    @Autowired
    private ApiTodayRankService todayRankService;
    @Autowired
    private ApiTotalRankService totalRankService;
    @Autowired
    private ApiWeekRankService weekRankService;
    @Autowired
    private ApiRecordPraiseService recordPraiseService;
    @Autowired
    private ApiRecordCommentService recordCommentService;
    @Autowired
    private ApiAdService adService;
    @Autowired
    private ApiUserService userService;
    
    /**
     * 打卡
     */
    @RequestMapping("add")
    public Map<String,Object> recordAdd(@LoginUser UserVo loginUser,@RequestBody RecordVo record) {
        try {
            return recordService.recordAdd(record, loginUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toResponsFail("提交失败");
    }
    
    /**
     * 点赞
     */
    @RequestMapping("praise")
    public Map<String,Object> recordPraise(@LoginUser UserVo loginUser,@RequestBody RecordPraiseVo recordPraise) {
    	Map<String,Object> params = new HashMap<String,Object>();
    	try {
    		if(loginUser.getRealname()==null) {
    			return toResponsFail("请补充个人信息");
    		}
    	   recordPraise.setUserName(loginUser.getRealname());
    	   params.put("userId", recordPraise.getUserId()); 
    	   params.put("recordId", recordPraise.getRecordId()); 
//  	       params.put("page", 1);
//  	       params.put("limit", 10);
  	       params.put("sidx", "id");
  	       params.put("order", "desc");
  	       //查询列表数据
//  	       Query query = new Query(params);
           List<RecordPraiseVo> recordlist = recordPraiseService.queryList(params);
           if(recordlist!=null && recordlist.size()>0) {
        	   return toResponsFail("已点赞");
           }
           recordPraise.setPraiseTime(new Date());
           recordPraise.setStatusId(1);
           
//           recordPraise.setStatusText("有效");
           recordPraiseService.save(recordPraise);
           return toResponsMsgSuccess("提交成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toResponsFail("提交失败");
    }
    
    /**
     * 评论
     */
    @RequestMapping("comment")
    public Map<String,Object> recordComment(@LoginUser UserVo loginUser,@RequestBody RecordCommentVo recordComment) {
        try {
        	if(loginUser.getRealname()==null) {
    			return toResponsFail("请补充个人信息");
    		}
    	   recordComment.setUserName(loginUser.getRealname());
        	recordComment.setCommentTime(new Date());
        	recordComment.setStatusId(1);
//        	recordComment.setStatusText("有效");
           recordCommentService.save(recordComment);
           return toResponsMsgSuccess("提交成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toResponsFail("提交失败");
    }
    
    /**
     * 删除自己的评论
     */
    @RequestMapping("comment/delete")
    public Map<String,Object> recordComment(@LoginUser UserVo loginUser,@RequestBody Long id) {
        try {
           recordCommentService.delete(id);
           return toResponsMsgSuccess("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toResponsFail("删除失败");
    }
    
    /**
     * 获取所有打卡列表
     */
    @IgnoreAuth
    @RequestMapping("list_all")
    public Object list_all(@LoginUser UserVo loginUser,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "size", defaultValue = "10") Integer size) {
    	Map<String, Object> resultObj = new HashMap<String,Object>();
        Map<String,Object> params = new HashMap<String,Object>();
        
//        params.put("userId", userId);
        params.put("page", page);
        params.put("limit", size);
        params.put("sidx", "id");
        params.put("order", "desc");
        //查询列表数据
        Query query = new Query(params);
        List<RecordVo> recordList = recordService.queryList(query);
        int total = recordService.queryTotal(query);
        //
        for (int i=0;i< recordList.size();i++) {
            Map<String,Object> recordCommentParam = new HashMap<String,Object>();
            recordCommentParam.put("recordId", recordList.get(i).getId());
            //获取点赞
            List<RecordPraiseVo> recordPraiseList = recordPraiseService.queryList(recordCommentParam);
            //获取评论
            List<RecordCommentVo> recordCommentList = recordCommentService.queryList(recordCommentParam);
            if(recordPraiseList!=null && recordPraiseList.size()>0) {
            	recordList.get(i).setRecordPraiseList(recordPraiseList);
            }
            if(recordCommentList!=null && recordCommentList.size()>0) {
            	recordList.get(i).setRecordCommentList(recordCommentList);
            }
        }
        Map<String,Object> bannerParams = new HashMap<String,Object>();
        bannerParams.put("ad_position_id", 1);
        List<AdVo> banner = adService.queryList(bannerParams);
        resultObj.put("banner", banner);
        ApiPageUtils pageUtil = new ApiPageUtils(recordList, total, query.getLimit(), query.getPage());
        resultObj.put("data",pageUtil);
        if(loginUser==null || loginUser.getUserId()==null) {
        	resultObj.put("totalRank",null);
        	resultObj.put("weekRank",null);
        	resultObj.put("loginUser", null);
        }else {
        	//总排行
            TotalRankVo totalRankVo = new TotalRankVo();
            totalRankVo.setUserId(loginUser.getUserId());
            List<TotalRankVo> totalRank = totalRankService.queryRankList(totalRankVo);
            if(totalRank!=null && totalRank.size()>0) {
//            	pageUtil.setTotalRank(totalRank.get(0));
            	resultObj.put("totalRank",totalRank.get(0));
            }else {
            	resultObj.put("totalRank",null);
            }
            //周排行
            WeekRankVo weekRankVo = new WeekRankVo();
            weekRankVo.setUserId(loginUser.getUserId());
            List<WeekRankVo> weekRank = weekRankService.queryRankList(weekRankVo);
            if(weekRank!=null && weekRank.size()>0) {
//            	pageUtil.setWeekRank(weekRank.get(0));
            	resultObj.put("weekRank",weekRank.get(0));
            }else {
            	resultObj.put("weekRank",null);
            }
            resultObj.put("loginUser", loginUser);
        }
//        pageUtil.setUser(loginUser);
        return toResponsSuccess(resultObj);
    }
    
    /**
     * 获取打卡列表
     */
    @RequestMapping("list_index")
    public Object indexlist(@LoginUser UserVo loginUser,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "size", defaultValue = "10") Integer size) {//@RequestParam(value = "userId") Long userId
    	
    	Map<String, Object> resultObj = new HashMap<String,Object>();
//        if(userId==null) {
          long	userId = loginUser.getUserId();
//        }
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("userId", userId);
        params.put("page", page);
        params.put("limit", size);
        params.put("sidx", "id");
        params.put("order", "desc");
        //查询列表数据
        Query query = new Query(params);
        List<RecordVo> recordList = recordService.queryList(query);
        int total = recordService.queryTotal(query);
        //
        for (int i=0;i< recordList.size();i++) {
            Map<String,Object> recordCommentParam = new HashMap<String,Object>();
            recordCommentParam.put("recordId", recordList.get(i).getId());
            //获取点赞
            List<RecordPraiseVo> recordPraiseList = recordPraiseService.queryList(recordCommentParam);
            //获取评论
            List<RecordCommentVo> recordCommentList = recordCommentService.queryList(recordCommentParam);
            if(recordPraiseList!=null && recordPraiseList.size()>0) {
            	recordList.get(i).setRecordPraiseList(recordPraiseList);
            }
            if(recordCommentList!=null && recordCommentList.size()>0) {
            	recordList.get(i).setRecordCommentList(recordCommentList);
            }
        }
        
        ApiPageUtils pageUtil = new ApiPageUtils(recordList, total, query.getLimit(), query.getPage());
        resultObj.put("data",pageUtil);
        //总排行
        TotalRankVo totalRankVo = new TotalRankVo();
        totalRankVo.setUserId(userId);//
        List<TotalRankVo> totalRank = totalRankService.queryRankList(totalRankVo);
        if(totalRank!=null && totalRank.size()>0) {
//        	pageUtil.setTotalRank(totalRank.get(0));resultObj.put("totalRank",totalRank.get(0));
        	resultObj.put("totalRank",totalRank.get(0));
        }else {
        	resultObj.put("totalRank",null);
        }
        //周排行
        WeekRankVo weekRankVo = new WeekRankVo();
        weekRankVo.setUserId(userId);//
        List<WeekRankVo> weekRank = weekRankService.queryRankList(weekRankVo);
        if(weekRank!=null && weekRank.size()>0) {
//        	pageUtil.setWeekRank(weekRank.get(0));
        	resultObj.put("weekRank",weekRank.get(0));
        }else {
        	resultObj.put("totalRank",null);
        }
//        pageUtil.setUser(loginUser);
        resultObj.put("loginUser", loginUser);
        Map<String,Object> bannerParams = new HashMap<String,Object>();
        bannerParams.put("ad_position_id", 1);
        List<AdVo> banner = adService.queryList(bannerParams);
        resultObj.put("banner", banner);
        return toResponsSuccess(resultObj);
    }
    
    /**
     * 获取打卡列表
     */
    @RequestMapping("list_self")
    public Object selflist(@LoginUser UserVo loginUser,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "size", defaultValue = "10") Integer size,
                       @RequestParam(value = "userId") Long userId) {
    	
    	Map<String, Object> resultObj = new HashMap<String,Object>();
        if(userId==null) {
          userId = loginUser.getUserId();
        }
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("userId", userId);
        params.put("page", page);
        params.put("limit", size);
        params.put("sidx", "id");
        params.put("order", "desc");
        //查询列表数据
        Query query = new Query(params);
        List<RecordVo> recordList = recordService.queryList(query);
        int total = recordService.queryTotal(query);
        //
        for (int i=0;i< recordList.size();i++) {
            Map<String,Object> recordCommentParam = new HashMap<String,Object>();
            recordCommentParam.put("recordId", recordList.get(i).getId());
            //获取点赞
            List<RecordPraiseVo> recordPraiseList = recordPraiseService.queryList(recordCommentParam);
            //获取评论
            List<RecordCommentVo> recordCommentList = recordCommentService.queryList(recordCommentParam);
            if(recordPraiseList!=null && recordPraiseList.size()>0) {
            	recordList.get(i).setRecordPraiseList(recordPraiseList);
            }
            if(recordCommentList!=null && recordCommentList.size()>0) {
            	recordList.get(i).setRecordCommentList(recordCommentList);
            }
        }
        
        ApiPageUtils pageUtil = new ApiPageUtils(recordList, total, query.getLimit(), query.getPage());
        resultObj.put("data",pageUtil);
        //总排行
        TotalRankVo totalRankVo = new TotalRankVo();
        totalRankVo.setUserId(userId);//
        List<TotalRankVo> totalRank = totalRankService.queryRankList(totalRankVo);
        if(totalRank!=null && totalRank.size()>0) {
//        	pageUtil.setTotalRank(totalRank.get(0));resultObj.put("totalRank",totalRank.get(0));
        	resultObj.put("totalRank",totalRank.get(0));
        }else {
        	resultObj.put("totalRank",null);
        }
        //周排行
        WeekRankVo weekRankVo = new WeekRankVo();
        weekRankVo.setUserId(userId);//
        List<WeekRankVo> weekRank = weekRankService.queryRankList(weekRankVo);
        if(weekRank!=null && weekRank.size()>0) {
//        	pageUtil.setWeekRank(weekRank.get(0));
        	resultObj.put("weekRank",weekRank.get(0));
        }else {
        	resultObj.put("totalRank",null);
        }
//        pageUtil.setUser(loginUser);
        resultObj.put("loginUser", loginUser);
        Map<String,Object> bannerParams = new HashMap<String,Object>();
        bannerParams.put("ad_position_id", 1);
        List<AdVo> banner = adService.queryList(bannerParams);
        resultObj.put("banner", banner);
        return toResponsSuccess(resultObj);
    }
    
    /**
     * 获取打卡列表
     */
    @RequestMapping("rank")
    public Object rank(@LoginUser UserVo loginUser,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "size", defaultValue = "10") Integer size) {
       
        Map<String,Object> params = new HashMap<String,Object>();
        JSONObject jsonParam = getJsonRequest();
        String rankType = jsonParam.getString("rankType");
        params.put("page", page);
        params.put("limit", size);
        params.put("sidx", "id");
        params.put("order", "desc");
        //查询列表数据
        Query query = new Query(params);
        ApiPageUtils pageUtil = new ApiPageUtils(null, 0, query.getLimit(), query.getPage());
        
        if("today".equals(rankType)) {
            TodayRankVo todayRankVo = new TodayRankVo();
            List<TodayRankVo> todayRankList = todayRankService.queryRankList(todayRankVo);
            pageUtil.setCount(todayRankList.size());
            pageUtil.setData(todayRankList);
           
            todayRankVo.setUserId(loginUser.getUserId());
            List<TodayRankVo> myTodayRankList = todayRankService.queryRankList(todayRankVo);
            if(myTodayRankList!=null && myTodayRankList.size()>0) {
            	pageUtil.setTodayRank(myTodayRankList.get(0));
            }
        }else if("week".equals(rankType)) {
        	//周排行
            WeekRankVo weekRankVo = new WeekRankVo();
            List<WeekRankVo> weekRank = weekRankService.queryRankList(weekRankVo);
            pageUtil.setCount(weekRank.size());
            pageUtil.setData(weekRank);
            
            weekRankVo.setUserId(loginUser.getUserId());//
            List<WeekRankVo> myWeekRank = weekRankService.queryRankList(weekRankVo);
            if(myWeekRank!=null && myWeekRank.size()>0) {
            	pageUtil.setWeekRank(myWeekRank.get(0));
            }
        }else if("total".equals(rankType)) {
        	//总排行
            TotalRankVo totalRankVo = new TotalRankVo();
            List<TotalRankVo> totalRank = totalRankService.queryRankList(totalRankVo);
            pageUtil.setCount(totalRank.size());
            pageUtil.setData(totalRank);
            
            totalRankVo.setUserId(loginUser.getUserId());//
            List<TotalRankVo> myTotalRank = totalRankService.queryRankList(totalRankVo);
            if(myTotalRank!=null && myTotalRank.size()>0) {
            	pageUtil.setTotalRank(myTotalRank.get(0));
            }
        }
        pageUtil.setUser(loginUser);
        return toResponsSuccess(pageUtil);
    }
    
    /**
     * 我要请假
     */
    @RequestMapping("/leave_save")
    public Map<String,Object> leave_save(@RequestBody LeaveVo leave,@LoginUser UserVo loginUser) {
        
    	try {
    	   if(loginUser.getIsleave()==1) {
    		   return toResponsFail("不可重复请假");
    	   }
    	   if(leave.getUserId()==null) {
    		   leave.setUserId(loginUser.getUserId());
    	   }
    	   if(leave.getUserName()==null) {
    		   leave.setUserName(loginUser.getRealname());
    	   }
    	  
    	   leave.setStartTime(new Date());
    	   UserVo user = new UserVo();
    	   user.setIsleave(1);
    	   user.setUserId(loginUser.getUserId());
    	   userService.update(user);
           int flag = leaveService.save(leave);
           if(flag>0) {
        	   return toResponsMsgSuccess("请假成功");
           }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toResponsFail("请假失败");
    }
    
    /**
     * 补充信息
     */
    @RequestMapping("/login_info")
    public Map<String,Object> login_info(@RequestBody UserVo user,@LoginUser UserVo loginUser) {
        
    	try {
    	   if(loginUser.getStatus()==1) {
    			return toResponsFail("用户审核中，不可重复提交");
    	   }
    	   if(user.getRealname()==null) {
    		   return toResponsFail("姓名不能为空");
    	   }
    	   if(user.getClassroom()==null) {
    		   return toResponsFail("班级不能为空");
    	   }
    	   if(user.getMobile()==null) {
    		   return toResponsFail("手机号不能为空");
    	   }
    	   loginUser.setStatus(1);//审核中
    	   loginUser.setRealname(user.getRealname());
    	   loginUser.setClassroom(user.getClassroom());
    	   loginUser.setMobile(user.getMobile());
    	   
           userService.update(loginUser);
           return toResponsMsgSuccess("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toResponsFail("操作失败");
    }

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    public Object list(@LoginUser UserVo loginUser,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size,
            @RequestParam(value = "userId",defaultValue = "") Long userId) {
    	
    	Map<String, Object> resultObj = new HashMap<String,Object>();
        if(userId==null) {
        	userId = loginUser.getUserId();
        }
//    	JSONObject jsonParam = getJsonRequest();
    	Map<String,Object> params = new HashMap<String,Object>();
        //查询列表数据
        
        params.put("userId", userId);
        params.put("page", page);
        params.put("limit", size);
        params.put("sidx", "id");
        params.put("order", "desc");
        Query query = new Query(params);
      //总排行
        TotalRankVo totalRankVo = new TotalRankVo();
        totalRankVo.setUserId(userId);//
        List<TotalRankVo> totalRank = totalRankService.queryRankList(totalRankVo);
        if(totalRank!=null && totalRank.size()>0) {
//        	pageUtil.setTotalRank(totalRank.get(0));resultObj.put("totalRank",totalRank.get(0));
        	resultObj.put("totalRank",totalRank.get(0));
        }else {
        	resultObj.put("totalRank",null);
        }
        //周排行
        WeekRankVo weekRankVo = new WeekRankVo();
        weekRankVo.setUserId(userId);//
        List<WeekRankVo> weekRank = weekRankService.queryRankList(weekRankVo);
        if(weekRank!=null && weekRank.size()>0) {
//        	pageUtil.setWeekRank(weekRank.get(0));
        	resultObj.put("weekRank",weekRank.get(0));
        }else {
        	resultObj.put("totalRank",null);
        }
        
        List<RecordVo> recordList = recordService.queryList(query);
        int total = recordService.queryTotal(query);
        PageUtils pageUtil = new PageUtils(recordList, total, query.getLimit(), query.getPage());
        resultObj.put("data", pageUtil);
        resultObj.put("loginUser", loginUser);
        return toResponsSuccess(resultObj);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("record:info")
    public R info(@PathVariable("id") Long id) {
        RecordVo record = recordService.queryObject(id);

        return R.ok().put("record", record);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("record:save")
    public R save(@RequestBody RecordVo record) {
        recordService.save(record);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("record:update")
    public R update(@RequestBody RecordVo record) {
        recordService.update(record);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long id) {
        recordService.delete(id);
        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<RecordVo> list = recordService.queryList(params);

        return R.ok().put("list", list);
    }
}

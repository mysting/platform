package com.platform.dao;

import com.platform.entity.LeaveEntity;

/**
 * 会员请假表Dao
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-08-19 22:17:31
 */
public interface LeaveDao extends BaseDao<LeaveEntity> {

}

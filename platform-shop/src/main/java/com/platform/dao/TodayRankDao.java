package com.platform.dao;

import com.platform.entity.TodayRankEntity;

/**
 * 今日排行表Dao
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
public interface TodayRankDao extends BaseDao<TodayRankEntity> {

}

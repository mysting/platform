package com.platform.dao;

import com.platform.entity.TotalRankEntity;

/**
 * 总排行表Dao
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:43
 */
public interface TotalRankDao extends BaseDao<TotalRankEntity> {

}

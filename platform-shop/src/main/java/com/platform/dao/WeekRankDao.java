package com.platform.dao;

import com.platform.entity.WeekRankEntity;

/**
 * 本周排行表Dao
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
public interface WeekRankDao extends BaseDao<WeekRankEntity> {

}

package com.platform.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * 今日排行表实体
 * 表名 gaojin_today_rank
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
public class TodayRankEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long id;
    //用户ID
    private Long userId;
    //用户姓名
    private String userName;
    //
    private Integer todayCount;
    //
    private Integer todayMinute;
    //创建时间
    private Date todayTime;

    /**
     * 设置：
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置：用户ID
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取：用户ID
     */
    public Long getUserId() {
        return userId;
    }
    /**
     * 设置：用户姓名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取：用户姓名
     */
    public String getUserName() {
        return userName;
    }
    /**
     * 设置：
     */
    public void setTodayCount(Integer todayCount) {
        this.todayCount = todayCount;
    }

    /**
     * 获取：
     */
    public Integer getTodayCount() {
        return todayCount;
    }
    /**
     * 设置：
     */
    public void setTodayMinute(Integer todayMinute) {
        this.todayMinute = todayMinute;
    }

    /**
     * 获取：
     */
    public Integer getTodayMinute() {
        return todayMinute;
    }
    /**
     * 设置：创建时间
     */
    public void setTodayTime(Date todayTime) {
        this.todayTime = todayTime;
    }

    /**
     * 获取：创建时间
     */
    public Date getTodayTime() {
        return todayTime;
    }
}

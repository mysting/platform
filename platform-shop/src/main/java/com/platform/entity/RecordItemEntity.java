package com.platform.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * 打卡子项表实体
 * 表名 gaojin_record_item
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:43
 */
public class RecordItemEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long id;
    //用户ID
    private Long recordId;
    //子项
    private Long itemId;
    //子项名称
    private String itemTypeText;
    //创建时间
    private Date createTime;
    //描述
    private String itemDesc;
    
    private String action;
    private Double loadWeight;
    private Integer group;
    private Integer number;

    /**
     * 设置：
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置：用户ID
     */
    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    /**
     * 获取：用户ID
     */
    public Long getRecordId() {
        return recordId;
    }
    /**
     * 设置：子项
     */
    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    /**
     * 获取：子项
     */
    public Long getItemId() {
        return itemId;
    }
    /**
     * 设置：子项名称
     */
    public void setItemTypeText(String itemTypeText) {
        this.itemTypeText = itemTypeText;
    }

    /**
     * 获取：子项名称
     */
    public String getItemTypeText() {
        return itemTypeText;
    }
    /**
     * 设置：创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取：创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }
    /**
     * 设置：描述
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    /**
     * 获取：描述
     */
    public String getItemDesc() {
        return itemDesc;
    }

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Double getLoadWeight() {
		return loadWeight;
	}

	public void setLoadWeight(Double loadWeight) {
		this.loadWeight = loadWeight;
	}

	public Integer getGroup() {
		return group;
	}

	public void setGroup(Integer group) {
		this.group = group;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}
}

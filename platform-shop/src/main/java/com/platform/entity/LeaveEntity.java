package com.platform.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * 会员请假表实体
 * 表名 gaojin_leave
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-08-19 22:17:31
 */
public class LeaveEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long id;
    //用户ID
    private Long userId;
    //用户姓名
    private String userName;
    //
    private Long days;
    //请假开始日期
    private Date startDate;
    //请假结束日期
    private Date endDate;
    //请假原因
    private String desc;
    //开始时间
    private Date startTime;
    //结束时间
    private Date endTime;

    /**
     * 设置：
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置：用户ID
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取：用户ID
     */
    public Long getUserId() {
        return userId;
    }
    /**
     * 设置：用户姓名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取：用户姓名
     */
    public String getUserName() {
        return userName;
    }
    /**
     * 设置：
     */
    public void setDays(Long days) {
        this.days = days;
    }

    /**
     * 获取：
     */
    public Long getDays() {
        return days;
    }
    /**
     * 设置：请假开始日期
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * 获取：请假开始日期
     */
    public Date getStartDate() {
        return startDate;
    }
    /**
     * 设置：请假结束日期
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * 获取：请假结束日期
     */
    public Date getEndDate() {
        return endDate;
    }
    /**
     * 设置：请假原因
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * 获取：请假原因
     */
    public String getDesc() {
        return desc;
    }
    /**
     * 设置：开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取：开始时间
     */
    public Date getStartTime() {
        return startTime;
    }
    /**
     * 设置：结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取：结束时间
     */
    public Date getEndTime() {
        return endTime;
    }
}

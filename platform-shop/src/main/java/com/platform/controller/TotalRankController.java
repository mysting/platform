package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.TotalRankEntity;
import com.platform.service.TotalRankService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * 总排行表Controller
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:43
 */
@RestController
@RequestMapping("totalrank")
public class TotalRankController {
    @Autowired
    private TotalRankService totalRankService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("totalrank:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<TotalRankEntity> totalRankList = totalRankService.queryList(query);
        int total = totalRankService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(totalRankList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("totalrank:info")
    public R info(@PathVariable("id") Integer id) {
        TotalRankEntity totalRank = totalRankService.queryObject(id);

        return R.ok().put("totalRank", totalRank);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("totalrank:save")
    public R save(@RequestBody TotalRankEntity totalRank) {
        totalRankService.save(totalRank);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("totalrank:update")
    public R update(@RequestBody TotalRankEntity totalRank) {
        totalRankService.update(totalRank);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("totalrank:delete")
    public R delete(@RequestBody Integer[]ids) {
        totalRankService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<TotalRankEntity> list = totalRankService.queryList(params);

        return R.ok().put("list", list);
    }
}

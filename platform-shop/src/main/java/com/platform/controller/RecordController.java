package com.platform.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.RecordEntity;
import com.platform.entity.UserEntity;
import com.platform.service.RecordService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;
import com.platform.utils.excel.ExcelExport;

/**
 * 打卡表Controller
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@RestController
@RequestMapping("record")
public class RecordController {
    @Autowired
    private RecordService recordService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("record:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<RecordEntity> recordList = recordService.queryList(query);
        int total = recordService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(recordList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("record:info")
    public R info(@PathVariable("id") Integer id) {
        RecordEntity record = recordService.queryObject(id);

        return R.ok().put("record", record);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("record:save")
    public R save(@RequestBody RecordEntity record) {
        recordService.save(record);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("record:update")
    public R update(@RequestBody RecordEntity record) {
        recordService.update(record);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("record:delete")
    public R delete(@RequestBody Integer[]ids) {
        recordService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<RecordEntity> list = recordService.queryList(params);

        return R.ok().put("list", list);
    }
    
    /**
     * 导出会员
     */
    @RequestMapping("/export")
    @RequiresPermissions("record:export")
    public R export(@RequestParam Map<String, Object> params, HttpServletResponse response) {

        List<RecordEntity> recordList = recordService.queryList(params);

        ExcelExport ee = new ExcelExport("打卡列表");

        String[] header = new String[]{"会员姓名", "类别名称","其他", "动作名称", "重量", "组数", "每组次数", "媒体类别名", "打卡分钟", "打卡时间"};

        List<Map<String, Object>> list = new ArrayList<>();

        if (recordList != null && recordList.size() != 0) {
            for (RecordEntity recordEntity : recordList) {
                LinkedHashMap<String, Object> map = new LinkedHashMap<>();
                map.put("userName", recordEntity.getUserName());
                map.put("recordTypeText", recordEntity.getRecordTypeText());
                map.put("desc", recordEntity.getDesc());
                map.put("action", recordEntity.getAction());
                map.put("loadWeight", recordEntity.getLoadWeight());
                map.put("group", recordEntity.getGroup());
                map.put("number", recordEntity.getNumber());
                map.put("mediaTypeText", recordEntity.getMediaTypeText());
                map.put("recordMinute", recordEntity.getRecordMinute());
                map.put("recordTime", recordEntity.getRecordTime());
                list.add(map);
            }
        }
        ee.addSheetByMap("打卡", list, header);
        ee.export(response);
        return R.ok();
    }
}

package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.WeekRankEntity;
import com.platform.service.WeekRankService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * 本周排行表Controller
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@RestController
@RequestMapping("weekrank")
public class WeekRankController {
    @Autowired
    private WeekRankService weekRankService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("weekrank:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<WeekRankEntity> weekRankList = weekRankService.queryList(query);
        int total = weekRankService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(weekRankList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("weekrank:info")
    public R info(@PathVariable("id") Integer id) {
        WeekRankEntity weekRank = weekRankService.queryObject(id);

        return R.ok().put("weekRank", weekRank);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("weekrank:save")
    public R save(@RequestBody WeekRankEntity weekRank) {
        weekRankService.save(weekRank);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("weekrank:update")
    public R update(@RequestBody WeekRankEntity weekRank) {
        weekRankService.update(weekRank);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("weekrank:delete")
    public R delete(@RequestBody Integer[]ids) {
        weekRankService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<WeekRankEntity> list = weekRankService.queryList(params);

        return R.ok().put("list", list);
    }
}

package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.TodayRankEntity;
import com.platform.service.TodayRankService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * 今日排行表Controller
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@RestController
@RequestMapping("todayrank")
public class TodayRankController {
    @Autowired
    private TodayRankService todayRankService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("todayrank:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<TodayRankEntity> todayRankList = todayRankService.queryList(query);
        int total = todayRankService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(todayRankList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("todayrank:info")
    public R info(@PathVariable("id") Integer id) {
        TodayRankEntity todayRank = todayRankService.queryObject(id);

        return R.ok().put("todayRank", todayRank);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("todayrank:save")
    public R save(@RequestBody TodayRankEntity todayRank) {
        todayRankService.save(todayRank);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("todayrank:update")
    public R update(@RequestBody TodayRankEntity todayRank) {
        todayRankService.update(todayRank);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("todayrank:delete")
    public R delete(@RequestBody Integer[]ids) {
        todayRankService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<TodayRankEntity> list = todayRankService.queryList(params);

        return R.ok().put("list", list);
    }
}

package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.RecordCommentEntity;
import com.platform.service.RecordCommentService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * 打卡评论表Controller
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@RestController
@RequestMapping("recordcomment")
public class RecordCommentController {
    @Autowired
    private RecordCommentService recordCommentService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("recordcomment:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<RecordCommentEntity> recordCommentList = recordCommentService.queryList(query);
        int total = recordCommentService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(recordCommentList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("recordcomment:info")
    public R info(@PathVariable("id") Integer id) {
        RecordCommentEntity recordComment = recordCommentService.queryObject(id);

        return R.ok().put("recordComment", recordComment);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("recordcomment:save")
    public R save(@RequestBody RecordCommentEntity recordComment) {
        recordCommentService.save(recordComment);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("recordcomment:update")
    public R update(@RequestBody RecordCommentEntity recordComment) {
        recordCommentService.update(recordComment);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("recordcomment:delete")
    public R delete(@RequestBody Integer[]ids) {
        recordCommentService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<RecordCommentEntity> list = recordCommentService.queryList(params);

        return R.ok().put("list", list);
    }
}

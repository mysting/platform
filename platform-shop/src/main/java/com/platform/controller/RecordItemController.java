package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.RecordItemEntity;
import com.platform.service.RecordItemService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * 打卡子项表Controller
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:43
 */
@RestController
@RequestMapping("recorditem")
public class RecordItemController {
    @Autowired
    private RecordItemService recordItemService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("recorditem:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<RecordItemEntity> recordItemList = recordItemService.queryList(query);
        int total = recordItemService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(recordItemList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("recorditem:info")
    public R info(@PathVariable("id") Integer id) {
        RecordItemEntity recordItem = recordItemService.queryObject(id);

        return R.ok().put("recordItem", recordItem);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("recorditem:save")
    public R save(@RequestBody RecordItemEntity recordItem) {
        recordItemService.save(recordItem);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("recorditem:update")
    public R update(@RequestBody RecordItemEntity recordItem) {
        recordItemService.update(recordItem);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("recorditem:delete")
    public R delete(@RequestBody Integer[]ids) {
        recordItemService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<RecordItemEntity> list = recordItemService.queryList(params);

        return R.ok().put("list", list);
    }
}

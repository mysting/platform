package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.RecordPraiseEntity;
import com.platform.service.RecordPraiseService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * 打卡点赞表Controller
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@RestController
@RequestMapping("recordpraise")
public class RecordPraiseController {
    @Autowired
    private RecordPraiseService recordPraiseService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("recordpraise:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<RecordPraiseEntity> recordPraiseList = recordPraiseService.queryList(query);
        int total = recordPraiseService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(recordPraiseList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("recordpraise:info")
    public R info(@PathVariable("id") Integer id) {
        RecordPraiseEntity recordPraise = recordPraiseService.queryObject(id);

        return R.ok().put("recordPraise", recordPraise);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("recordpraise:save")
    public R save(@RequestBody RecordPraiseEntity recordPraise) {
        recordPraiseService.save(recordPraise);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("recordpraise:update")
    public R update(@RequestBody RecordPraiseEntity recordPraise) {
        recordPraiseService.update(recordPraise);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("recordpraise:delete")
    public R delete(@RequestBody Integer[]ids) {
        recordPraiseService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<RecordPraiseEntity> list = recordPraiseService.queryList(params);

        return R.ok().put("list", list);
    }
}

package com.platform.service;

import com.platform.entity.LeaveEntity;

import java.util.List;
import java.util.Map;

/**
 * 会员请假表Service接口
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-08-19 22:17:31
 */
public interface LeaveService {

    /**
     * 根据主键查询实体
     *
     * @param id 主键
     * @return 实体
     */
    LeaveEntity queryObject(Long id);

    /**
     * 分页查询
     *
     * @param map 参数
     * @return list
     */
    List<LeaveEntity> queryList(Map<String, Object> map);

    /**
     * 分页统计总数
     *
     * @param map 参数
     * @return 总数
     */
    int queryTotal(Map<String, Object> map);

    /**
     * 保存实体
     *
     * @param leave 实体
     * @return 保存条数
     */
    int save(LeaveEntity leave);

    /**
     * 根据主键更新实体
     *
     * @param leave 实体
     * @return 更新条数
     */
    int update(LeaveEntity leave);

    /**
     * 根据主键删除
     *
     * @param id
     * @return 删除条数
     */
    int delete(Long id);

    /**
     * 根据主键批量删除
     *
     * @param ids
     * @return 删除条数
     */
    int deleteBatch(Long[]ids);
}

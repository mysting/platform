package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.RecordItemDao;
import com.platform.entity.RecordItemEntity;
import com.platform.service.RecordItemService;

/**
 * 打卡子项表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:43
 */
@Service("recordItemService")
public class RecordItemServiceImpl implements RecordItemService {
    @Autowired
    private RecordItemDao recordItemDao;

    @Override
    public RecordItemEntity queryObject(Integer id) {
        return recordItemDao.queryObject(id);
    }

    @Override
    public List<RecordItemEntity> queryList(Map<String, Object> map) {
        return recordItemDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return recordItemDao.queryTotal(map);
    }

    @Override
    public int save(RecordItemEntity recordItem) {
        return recordItemDao.save(recordItem);
    }

    @Override
    public int update(RecordItemEntity recordItem) {
        return recordItemDao.update(recordItem);
    }

    @Override
    public int delete(Integer id) {
        return recordItemDao.delete(id);
    }

    @Override
    public int deleteBatch(Integer[]ids) {
        return recordItemDao.deleteBatch(ids);
    }
}

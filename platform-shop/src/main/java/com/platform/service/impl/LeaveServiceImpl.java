package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.LeaveDao;
import com.platform.entity.LeaveEntity;
import com.platform.service.LeaveService;

/**
 * 会员请假表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-08-19 22:17:31
 */
@Service("leaveService")
public class LeaveServiceImpl implements LeaveService {
    @Autowired
    private LeaveDao leaveDao;

    @Override
    public LeaveEntity queryObject(Long id) {
        return leaveDao.queryObject(id);
    }

    @Override
    public List<LeaveEntity> queryList(Map<String, Object> map) {
        return leaveDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return leaveDao.queryTotal(map);
    }

    @Override
    public int save(LeaveEntity leave) {
        return leaveDao.save(leave);
    }

    @Override
    public int update(LeaveEntity leave) {
        return leaveDao.update(leave);
    }

    @Override
    public int delete(Long id) {
        return leaveDao.delete(id);
    }

    @Override
    public int deleteBatch(Long[]ids) {
        return leaveDao.deleteBatch(ids);
    }
}

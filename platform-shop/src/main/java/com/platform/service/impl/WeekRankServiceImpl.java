package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.WeekRankDao;
import com.platform.entity.WeekRankEntity;
import com.platform.service.WeekRankService;

/**
 * 本周排行表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@Service("weekRankService")
public class WeekRankServiceImpl implements WeekRankService {
    @Autowired
    private WeekRankDao weekRankDao;

    @Override
    public WeekRankEntity queryObject(Integer id) {
        return weekRankDao.queryObject(id);
    }

    @Override
    public List<WeekRankEntity> queryList(Map<String, Object> map) {
        return weekRankDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return weekRankDao.queryTotal(map);
    }

    @Override
    public int save(WeekRankEntity weekRank) {
        return weekRankDao.save(weekRank);
    }

    @Override
    public int update(WeekRankEntity weekRank) {
        return weekRankDao.update(weekRank);
    }

    @Override
    public int delete(Integer id) {
        return weekRankDao.delete(id);
    }

    @Override
    public int deleteBatch(Integer[]ids) {
        return weekRankDao.deleteBatch(ids);
    }
}

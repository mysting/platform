package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.RecordCommentDao;
import com.platform.entity.RecordCommentEntity;
import com.platform.service.RecordCommentService;

/**
 * 打卡评论表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@Service("recordCommentService")
public class RecordCommentServiceImpl implements RecordCommentService {
    @Autowired
    private RecordCommentDao recordCommentDao;

    @Override
    public RecordCommentEntity queryObject(Integer id) {
        return recordCommentDao.queryObject(id);
    }

    @Override
    public List<RecordCommentEntity> queryList(Map<String, Object> map) {
        return recordCommentDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return recordCommentDao.queryTotal(map);
    }

    @Override
    public int save(RecordCommentEntity recordComment) {
        return recordCommentDao.save(recordComment);
    }

    @Override
    public int update(RecordCommentEntity recordComment) {
        return recordCommentDao.update(recordComment);
    }

    @Override
    public int delete(Integer id) {
        return recordCommentDao.delete(id);
    }

    @Override
    public int deleteBatch(Integer[]ids) {
        return recordCommentDao.deleteBatch(ids);
    }
}

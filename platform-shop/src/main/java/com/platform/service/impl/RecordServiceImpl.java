package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.RecordDao;
import com.platform.entity.RecordEntity;
import com.platform.service.RecordService;

/**
 * 打卡表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@Service("recordService")
public class RecordServiceImpl implements RecordService {
    @Autowired
    private RecordDao recordDao;

    @Override
    public RecordEntity queryObject(Integer id) {
        return recordDao.queryObject(id);
    }

    @Override
    public List<RecordEntity> queryList(Map<String, Object> map) {
        return recordDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return recordDao.queryTotal(map);
    }

    @Override
    public int save(RecordEntity record) {
        return recordDao.save(record);
    }

    @Override
    public int update(RecordEntity record) {
        return recordDao.update(record);
    }

    @Override
    public int delete(Integer id) {
        return recordDao.delete(id);
    }

    @Override
    public int deleteBatch(Integer[]ids) {
        return recordDao.deleteBatch(ids);
    }
}

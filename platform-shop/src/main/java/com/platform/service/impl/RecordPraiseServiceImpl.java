package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.RecordPraiseDao;
import com.platform.entity.RecordPraiseEntity;
import com.platform.service.RecordPraiseService;

/**
 * 打卡点赞表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@Service("recordPraiseService")
public class RecordPraiseServiceImpl implements RecordPraiseService {
    @Autowired
    private RecordPraiseDao recordPraiseDao;

    @Override
    public RecordPraiseEntity queryObject(Integer id) {
        return recordPraiseDao.queryObject(id);
    }

    @Override
    public List<RecordPraiseEntity> queryList(Map<String, Object> map) {
        return recordPraiseDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return recordPraiseDao.queryTotal(map);
    }

    @Override
    public int save(RecordPraiseEntity recordPraise) {
        return recordPraiseDao.save(recordPraise);
    }

    @Override
    public int update(RecordPraiseEntity recordPraise) {
        return recordPraiseDao.update(recordPraise);
    }

    @Override
    public int delete(Integer id) {
        return recordPraiseDao.delete(id);
    }

    @Override
    public int deleteBatch(Integer[]ids) {
        return recordPraiseDao.deleteBatch(ids);
    }
}

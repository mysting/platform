package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.TodayRankDao;
import com.platform.entity.TodayRankEntity;
import com.platform.service.TodayRankService;

/**
 * 今日排行表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:42
 */
@Service("todayRankService")
public class TodayRankServiceImpl implements TodayRankService {
    @Autowired
    private TodayRankDao todayRankDao;

    @Override
    public TodayRankEntity queryObject(Integer id) {
        return todayRankDao.queryObject(id);
    }

    @Override
    public List<TodayRankEntity> queryList(Map<String, Object> map) {
        return todayRankDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return todayRankDao.queryTotal(map);
    }

    @Override
    public int save(TodayRankEntity todayRank) {
        return todayRankDao.save(todayRank);
    }

    @Override
    public int update(TodayRankEntity todayRank) {
        return todayRankDao.update(todayRank);
    }

    @Override
    public int delete(Integer id) {
        return todayRankDao.delete(id);
    }

    @Override
    public int deleteBatch(Integer[]ids) {
        return todayRankDao.deleteBatch(ids);
    }
}

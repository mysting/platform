package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.TotalRankDao;
import com.platform.entity.TotalRankEntity;
import com.platform.service.TotalRankService;

/**
 * 总排行表Service实现类
 *
 * @author minyi
 * @email 460632205@qq.com
 * @date 2018-07-06 16:43:43
 */
@Service("totalRankService")
public class TotalRankServiceImpl implements TotalRankService {
    @Autowired
    private TotalRankDao totalRankDao;

    @Override
    public TotalRankEntity queryObject(Integer id) {
        return totalRankDao.queryObject(id);
    }

    @Override
    public List<TotalRankEntity> queryList(Map<String, Object> map) {
        return totalRankDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return totalRankDao.queryTotal(map);
    }

    @Override
    public int save(TotalRankEntity totalRank) {
        return totalRankDao.save(totalRank);
    }

    @Override
    public int update(TotalRankEntity totalRank) {
        return totalRankDao.update(totalRank);
    }

    @Override
    public int delete(Integer id) {
        return totalRankDao.delete(id);
    }

    @Override
    public int deleteBatch(Integer[]ids) {
        return totalRankDao.deleteBatch(ids);
    }
}

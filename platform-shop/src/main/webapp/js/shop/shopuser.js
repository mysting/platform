$(function () {
	function transUserStatus(value){
		if (value == 0) {
			return '<span class="label label-danger">未审核</span>';
	    }else if(value == 1){
	    	return '<span class="label label-info">审核中</span>';
	    }else if(value == 2){
	    	return '<span class="label label-success">已审核</span>';
	    }
		return '<span class="label label-danger">未审核</span>';
	}
	function transIsleave(value) {
	    if (value == 1) {
	        return '<span class="label label-success">已请假</span>';
	    }
	    return '<span class="label label-danger">未请假</span>';
	}
	function transIspay(value) {
	    if (value == 1) {
	        return '<span class="label label-success">已缴费</span>';
	    }
	    return '<span class="label label-danger">未缴费</span>';
	}
    $("#jqGrid").jqGrid({
        url: '../user/list',
        datatype: "json",
        colModel: [{
            label: 'id', name: 'id', index: 'id', key: true, hidden: true
        }, {
            label: '真实姓名', name: 'realname', index: 'realname', width: 80
        }, {
            label: '会员名称', name: 'username', index: 'username', width: 80
        }, {
            label: '会员密码', name: 'password', index: 'password', hidden: true
        }, {
            label: '性别', name: 'gender', index: 'gender', width: 40, formatter: function (value) {
                return transGender(value);
            }
        }, {
            label: '出生日期', name: 'birthday', index: 'birthday', width: 80, formatter: function (value) {
                return transDate(value);
            }
        }, {
            label: '微信名', name: 'nickname', index: 'nickname', width: 80
        }, {
            label: '手机号码', name: 'mobile', index: 'mobile', width: 120
        }, {
            label: '班级', name: 'classroom', index: 'classroom', width: 120
        }, {
            label: '用户状态', name: 'status', index: 'status', width: 80, formatter: function (value) {
                return transUserStatus(value);
            }
        }, {
            label: '请假状态', name: 'isleave', index: 'isleave', width: 80, formatter: function (value) {
                return transIsleave(value);
            }
        }, {
            label: '缴费状态', name: 'ispay', index: 'ispay', width: 80, formatter: function (value) {
                return transIspay(value);
            }
        }, {
            label: '缴费金额', name: 'payMoney', index: 'pay_money', width: 80
        }, {
            label: '注册时间', name: 'registerTime', index: 'register_time', width: 80, formatter: function (value) {
                return transDate(value);
            }
        }, {
            label: '最后登录时间', name: 'lastLoginTime', index: 'last_login_time', width: 80, formatter: function (value) {
                return transDate(value);
            }
        }, {
            label: '最后登录Ip', name: 'lastLoginIp', index: 'last_login_ip', hidden: true
        }, {
            label: '会员等级', name: 'levelName', index: 'level_name', hidden: true
        }, {
            label: '注册Ip', name: 'registerIp', index: 'register_ip', hidden: true
        }, {
            label: '头像', name: 'avatar', index: 'avatar', width: 80, formatter: function (value) {
                return transImg(value);
            }
        }, {
            label: '微信Id', name: 'weixinOpenid', index: 'weixin_openid', width: 80, hidden: true
        }],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList: [10, 30, 50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth: true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader: {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames: {
            page: "page",
            rows: "limit",
            order: "order"
        },
        gridComplete: function () {
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({"overflow-x": "hidden"});
        }
    });
});

var vm = new Vue({
    el: '#rrapp',
    data: {
        showList: true,
        title: null,
        user: {
            gender: 1,
            status:0,
            isleave:0,
            ispay:0
        },
        ruleValidate: {
            username: [
                {required: true, message: '会员名称不能为空', trigger: 'blur'}
            ]
        },
        q: {
            username: ''
        },
        userLevels: []
    },
    methods: {
        query: function () {
            vm.reload();
        },
        add: function () {
            vm.showList = false;
            vm.title = "新增";
            vm.user = {gender: 1,status:0,isleave:0,ispay:0};
            vm.userLevels = [];

            this.getUserLevels();
        },
        update: function (event) {
            var id = getSelectedRow();
            if (id == null) {
                return;
            }
            vm.showList = false;
            vm.title = "修改";

            vm.getInfo(id)
            this.getUserLevels();
        },
        saveOrUpdate: function (event) {
            var url = vm.user.id == null ? "../user/save" : "../user/update";
            $.ajax({
                type: "POST",
                url: url,
                contentType: "application/json",
                data: JSON.stringify(vm.user),
                success: function (r) {
                    if (r.code === 0) {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
                    } else {
                        alert(r.msg);
                    }
                }
            });
        },
        del: function (event) {
            var ids = getSelectedRows();
            if (ids == null) {
                return;
            }

            confirm('确定要删除选中的记录？', function () {
                $.ajax({
                    type: "POST",
                    url: "../user/delete",
                    contentType: "application/json",
                    data: JSON.stringify(ids),
                    success: function (r) {
                        if (r.code == 0) {
                            alert('操作成功', function (index) {
                                $("#jqGrid").trigger("reloadGrid");
                            });
                        } else {
                            alert(r.msg);
                        }
                    }
                });
            });
        },
        exportUser: function () {
            exportFile('#rrapp', '../user/export', {'username': vm.q.username});
        },
        coupon: function () {
            var id = getSelectedRow();
            if (id == null) {
                return;
            }
            openWindow({
                title: '优惠券',
                type: 2,
                content: '../shop/usercoupon.html?userId=' + id
            })
        },
        address: function () {
            var id = getSelectedRow();
            if (id == null) {
                return;
            }
            openWindow({
                title: '收获地址',
                type: 2,
                content: '../shop/address.html?userId=' + id
            })
        },
        shopCart: function () {
            var id = getSelectedRow();
            if (id == null) {
                return;
            }
            openWindow({
                title: '购物车',
                type: 2,
                content: '../shop/cart.html?userId=' + id
            })
        },
        getInfo: function (id) {
            $.get("../user/info/" + id, function (r) {
                vm.user = r.user;
            });
        },
        /**
         * 获取会员级别
         */
        getUserLevels: function () {
            $.get("../userlevel/queryAll", function (r) {
                vm.userLevels = r.list;
            });
        },
        reload: function (event) {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam', 'page');
            $("#jqGrid").jqGrid('setGridParam', {
                postData: {'username': vm.q.username},
                page: page,
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
        },
        handleSubmit: function (name) {
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        }
    }
});
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

public class MyTest {
	public static void main(String[] args) {
		 Date nowTime = new Date();
		 LocalDate inputDate = LocalDate.parse("20180715");
		 TemporalAdjuster FIRST_OF_WEEK = TemporalAdjusters.ofDateAdjuster(localDate -> localDate.minusDays(localDate.getDayOfWeek().getValue()-DayOfWeek.MONDAY.getValue()));
		 System.out.println(inputDate.with(FIRST_OF_WEEK));
		 TemporalAdjuster LAST_OF_WEEK = TemporalAdjusters.ofDateAdjuster(localDate -> localDate.plusDays(DayOfWeek.SUNDAY.getValue() - localDate.getDayOfWeek().getValue()));
		 System.out.println(inputDate.with(LAST_OF_WEEK));
	}
}

$(function () {
    $("#jqGrid").jqGrid({
        url: '../record/list',
        datatype: "json",
        colModel: [
			{label: 'id', name: 'id', index: 'id', key: true, hidden: true},
			{label: '用户ID', name: 'userId', index: 'user_id', width: 80,hidden: true},
			{label: '会员姓名', name: 'userName', index: 'user_name', width: 80},
			{label: '打卡类别', name: 'recordTypeId', index: 'record_type_id', width: 80,hidden: true},
			{label: '类别名称', name: 'recordTypeText', index: 'record_type_text', width: 80},
			{label: '其他', name: 'desc', index: 'desc', width: 80},
			{label: '动作名称', name: 'action', index: 'action', width: 80, hidden: true},
			{label: '重量', name: 'loadWeight', index: 'load_weight', width: 80, hidden: true},
			{label: '组数', name: 'group', index: 'group', width: 80, hidden: true},
			{label: '每组次数', name: 'number', index: 'number', width: 80, hidden: true},
			{label: '媒体类别', name: 'mediaTypeId', index: 'media_type_id', width: 80,hidden: true},
			{label: '媒体类别名', name: 'mediaTypeText', index: 'media_type_text', width: 80},
			{label: '打卡分钟', name: 'recordMinute', index: 'record_minute', width: 80},
			{label: '状态值', name: 'statusId', index: 'status_id', width: 80,hidden: true},
			{label: '状态值名', name: 'statusText', index: 'status_text', width: 80,hidden: true},
			{label: '媒体url', name: 'mediaUrl', index: 'media_url', width: 80},
			{label: '纬度', name: 'latitude', index: 'latitude', width: 80,hidden: true},
			{label: '经度', name: 'longitude', index: 'longitude', width: 80,hidden: true},
			{label: '地址', name: 'address', index: 'address', width: 80,hidden: true},
			{label: '创建时间', name: 'createTime', index: 'create_time', width: 80,hidden: true},
			{label: '打卡时间', name: 'recordTime', index: 'record_time', width: 80, formatter: function (value) {
                return transDate(value);
            }},
			{label: '审核时间', name: 'confirmTime', index: 'confirm_time', width: 80,hidden: true}
		],
		viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList: [10, 30, 50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth: true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader: {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames: {
            page: "page",
            rows: "limit",
            order: "order"
        },
        gridComplete: function () {
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({"overflow-x": "hidden"});
        }
    });
});

let vm = new Vue({
	el: '#rrapp',
	data: {
        showList: true,
        title: null,
		record: {},
		ruleValidate: {
			name: [
				{required: true, message: '名称不能为空', trigger: 'blur'}
			]
		},
		q: {
			name: ''
		}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function () {
			vm.showList = false;
			vm.title = "新增";
			vm.record = {};
		},
		update: function (event) {
            let id = getSelectedRow();
			if (id == null) {
				return;
			}
			vm.showList = false;
            vm.title = "修改";

            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
            let url = vm.record.id == null ? "../record/save" : "../record/update";
			$.ajax({
				type: "POST",
			    url: url,
			    contentType: "application/json",
			    data: JSON.stringify(vm.record),
                success: function (r) {
                    if (r.code === 0) {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
                    } else {
                        alert(r.msg);
                    }
                }
			});
		},
		del: function (event) {
            let ids = getSelectedRows();
			if (ids == null){
				return;
			}

			confirm('确定要删除选中的记录？', function () {
				$.ajax({
					type: "POST",
				    url: "../record/delete",
				    contentType: "application/json",
				    data: JSON.stringify(ids),
				    success: function (r) {
						if (r.code == 0) {
							alert('操作成功', function (index) {
								$("#jqGrid").trigger("reloadGrid");
							});
						} else {
							alert(r.msg);
						}
					}
				});
			});
		},
		exportRecord: function () {
            exportFile('#rrapp', '../record/export', {'username': vm.q.name});
        },
		getInfo: function(id){
			$.get("../record/info/"+id, function (r) {
                vm.record = r.record;
            });
		},
		reload: function (event) {
			vm.showList = true;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
			$("#jqGrid").jqGrid('setGridParam', {
                postData: {'userName': vm.q.name},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
		},
        handleSubmit: function (name) {
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        }
	}
});
var api = require('../config/api.js');

function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}


function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function getFormatDate() {
  var date = new Date()
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  return [year, month, day].map(formatNumber).join('-')
}

function getDiffDate(startDate,endDate) {

  var start = Date.parse(new Date(startDate))
  var end = Date.parse(new Date(endDate))
  var day = 1000 * 60 * 60 * 24
  var diffValue = end - start
	if(diffValue < 0){return 0}
  var result = diffValue/day + 1
  return result
}



//转换标准时间为时间戳：
function getDateTimeStamp(dateStr){
  return Date.parse(dateStr.replace(/-/gi,"/"));
 }

function timetrans(timestamp){
  // var timestamp = new Date().getTime();
  var date = new Date(timestamp*1000);//如果date为13位不需要乘1000
  var Y = date.getFullYear() + '-';
  var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
  var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
  var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
  var m = (date.getMinutes() <10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
  var s = (date.getSeconds() <10 ? '0' + date.getSeconds() : date.getSeconds());
  return Y+M+D+h+m+s;
}

/**
 * 人性化时间处理 传入时间戳
 */
function T(timestamp){
  var mistiming=Math.round(new Date() / 1000)-timestamp;
  var arrr = ['年','个月','星期','天','小时','分钟','秒'];
  var arrn = [31536000,2592000,604800,86400,3600,60,1];
  for(var i=6;i>=0;i--){
      var inm = Math.floor(mistiming/arrn[i]);
      if(inm!=0){
          return inm+arrr[i]+'前';
      }
  }
}

function getDateDiff(dateTimeStamp){
  var result = "";
  var second = 1000;
	var minute = 1000 * 60;
	var hour = minute * 60;
	var day = hour * 24;
	var halfamonth = day * 15;
	var month = day * 30;
	var now = new Date().getTime();
	var diffValue = now - dateTimeStamp;
	if(diffValue < 0){return;}
	var monthC =diffValue/month;
	var weekC =diffValue/(7*day);
	var dayC =diffValue/day;
	var hourC =diffValue/hour;
  var minC =diffValue/minute;
  var secC =diffValue/second;
  
	if(monthC>=1){
		result="" + parseInt(monthC) + "月前";
	}
	else if(weekC>=1){
		result="" + parseInt(weekC) + "周前";
	}
	else if(dayC>=1){
		result=""+ parseInt(dayC) +"天前";
	}
	else if(hourC>=1){
		result=""+ parseInt(hourC) +"小时前";
	}
	else if(minC>=1){
		result=""+ parseInt(minC) +"分钟前";
	}else if(secC>=1){
    result=""+ parseInt(secC) +"秒前";
  }else{
    result="刚刚";
  }
	return result;
}

/**
  *
  * json转字符串
  */
function stringToJson(data){
  return JSON.parse(data);
}
/**
*字符串转json
*/
function jsonToString(data){
  return JSON.stringify(data);
}
/**
*map转换为json
*/
function mapToJson(map) {
return JSON.stringify(strMapToObj(map));
}
/**
*json转换为map
*/
function jsonToMap(jsonStr){
  return  objToStrMap(JSON.parse(jsonStr));
}


/**
*map转化为对象（map所有键都是字符串，可以将其转换为对象）
*/
function strMapToObj(strMap){
  let obj= Object.create(null);
  for (let[k,v] of strMap) {
    obj[k] = v;
  }
  return obj;
}

/**
*对象转换为Map
*/
function  objToStrMap(obj){
  let strMap = new Map();
  for (let k of Object.keys(obj)) {
    strMap.set(k,obj[k]);
  }
  return strMap;
}

/**
 * 封封微信的的request
 */
function request(url, data = {}, method = "GET") {
  return new Promise(function (resolve, reject) {
    wx.request({
      url: url,
      data: data,
      method: method,
      header: {
        'Content-Type': 'application/json',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        console.log("success");

        if (res.statusCode == 200) {

          if (res.data.errno == 401) {
            //需要登录后才可以操作
            wx.showModal({
                title: '',
                content: '请先登录',
                success: function (res){
                    if (res.confirm) {
                        wx.switchTab({
                          url: '/pages/ucenter/index/index'
                          // url: '/pages/index/index'
                        });
                    }
                }
            });
          } else {
            resolve(res.data);
          }
        } else {
          reject(res.errMsg);
        }

      },
      fail: function (err) {
        reject(err)
        console.log("failed")
      }
    })
  });
}

/**
 * 检查微信会话是否过期
 */
function checkSession() {
  return new Promise(function (resolve, reject) {
    wx.checkSession({
      success: function () {
        resolve(true);
      },
      fail: function () {
        reject(false);
      }
    })
  });
}

/**
 * 调用微信登录
 */
function login() {
  return new Promise(function (resolve, reject) {
    wx.login({
      success: function (res) {
        if (res.code) {
          //登录远程服务器
          console.log(res)
          resolve(res);
        } else {
          reject(res);
        }
      },
      fail: function (err) {
        reject(err);
      }
    });
  });
}

/**
 * 调用微信登录
 */
 
function getUserInfo() {
  return new Promise(function (resolve, reject) {

    // 查看是否授权
    wx.getSetting({
      success: function(res){
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          wx.getUserInfo({
            success: function(res) {
              console.log(res.userInfo)

            }
          })
          resolve(res);
        }else {
          reject(res);
        }
      },
      fail: function (err) {
        reject(err);
      }
    })
  });
}

function uploadImage(url){
  var apiUrl = url;
  return new Promise(function (resolve, reject) {
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function(res) {
        var tempFilePaths = res.tempFilePaths
        console.log(tempFilePaths)
        wx.uploadFile({
          url: apiUrl, //仅为示例，非真实的接口地址
          filePath: tempFilePaths[0],
          name: 'file',
          // formData:{
          //   'user': 'test'
          // },
          success: function(res){
            //do something
            console.log(res)
            resolve(stringToJson(res.data));
          },
          fail: function (err) {
            reject(err);
          }
        })
      }
    })
  });
}
function uploadVideo(url){
  var apiUrl = url;
  return new Promise(function (resolve, reject) {
    wx.chooseVideo({
        sourceType: ['album', 'camera'],
        maxDuration: 60,
        camera: 'back',
        success: function (res) {
          var tempFilePath = res.tempFilePath
          console.log(tempFilePath)
          wx.uploadFile({
            url: apiUrl, //仅为示例，非真实的接口地址
            filePath: tempFilePath,
            name: 'file',
            success: function(res){
              //do something
              console.log(res)
              resolve(stringToJson(res.data));
            },
            fail: function (err) {
              reject(err);
            }
          })
        }
    })
  });
}

function redirect(url) {

  //判断页面是否需要登录
  if (false) {
    wx.redirectTo({
      url: '/pages/auth/login/login'
    });
    return false;
  } else {
    wx.redirectTo({
      url: url
    });
  }
}

function showErrorToast(msg) {
  wx.showToast({
    title: msg,
    image: '/static/images/icon_error.png'
  })
}

function getRandomColor () {
  let rgb = []
  for (let i = 0 ; i < 3; ++i){
    let color = Math.floor(Math.random() * 256).toString(16)
    color = color.length == 1 ? '0' + color : color
    rgb.push(color)
  }
  return '#' + rgb.join('')
}

module.exports = {
  formatTime,
  getFormatDate,
  getDiffDate,
  timetrans,
  getDateTimeStamp,
  getDateDiff,
  request,
  redirect,
  showErrorToast,
  checkSession,
  login,
  getUserInfo,
  getRandomColor,
  uploadImage,
  uploadVideo,
  stringToJson,
  jsonToString,
  mapToJson,
  jsonToMap,
  strMapToObj,
  objToStrMap,
}



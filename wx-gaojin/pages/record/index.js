var app = getApp();
var util = require('../../utils/util.js');
var api = require('../../config/api.js');
Page({
  data: {
    videoWidth:null,
    videoHeight:null,
    userInfo:null,
    inputValue:'',
    loading:false,
    plain:false,
    disabled:false,
    mediaTypeId:[10,20],
    mediaTypeText:['image','video'],
    mediaTypeName:['图片','视频'],
    indexMT:0,
    indexT:0,
    recordTypeId:[10,20,30,40,50,60,70],
    recordTypeText:['有氧','抗阻','羽毛球','高尔夫球','游泳','舞蹈','其他'],
    //avatarUrl:'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIZGu5BWDoZKmcPXx0mZiaRQSdiadOKSsXe2S3Q0DkcEPMPZoqUoajETudZBlWs8icCg4W3RPIZ5F7nA/132',
    srcDefault:'../../static/images/default.png',
    // srcDefault:'https://my20180418-1255704432.cossh.myqcloud.com/upload/20180714/1720518578e556.jpg',
    // srcDefault:'https://my20180418-1255704432.cossh.myqcloud.com/upload/20180711/112626531d553f.mp4',
    defaultUrl:"../../static/images/user.png",
    mediaUrl:'',
    // mediaUrl:'https://my20180418-1255704432.cossh.myqcloud.com/upload/20180711/112626531d553f.mp4',
    typeId: 0,
    valueId: 0,
    content: '',
    recordMinute:'',
    action:'',
    loadWeight:null,
    group:null,
    number:null,
    desc:'',
    isShow:true,
    content:'',
    contentLength:0,
    itemList: [{action:'1',loadWeight:'2',group:'3',number:'4'}] 
  },
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
      that.setData({
        videoWidth: res.windowWidth * 0.8 * 2,
        videoHeight: parseInt(res.windowWidth * 0.8 * 0.75 * 2)
      });
      },
      fail: function(res) {}
    });

    console.log(app.globalData.userInfo)
    var that = this;
    that.setData({
      userInfo:app.globalData.userInfo
    })

    if(app.globalData.token==''&& app.globalData.userInfo==null){
      //需要登录后才可以操作
      wx.showModal({
        title: '',
        content: '请先登录',
        success: function (res){
          if (res.confirm) {
            wx.switchTab({
              url: '/pages/ucenter/index/index'
            });
          }else{
            wx.switchTab({
              url: '/pages/index/index'
            });
          }
        }
      });
    }else if(app.globalData.user && app.globalData.user.status<1){
      //需要登录后才可以操作
      wx.showModal({
        title: '',
        content: '请先补充个人信息',
        success: function (res){
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/auth/loginInfo/index'
            });
          }else{
            wx.switchTab({
              url: '/pages/index/index'
            });
          }
        }
      });
    }
    // that.setData({
    //   typeId: parseInt(options.typeId),
    //   valueId: parseInt(options.valueId)
    // });
  },
  onClose() {
    wx.navigateBack({
      delta: 1
    });
  },
  onPost() {
    let that = this;
    if (!this.data.content) {
      util.showErrorToast('请填写评论')
      return false;
    }
    util.request(api.CommentPost, {
      typeId: that.data.typeId,
      valueId: that.data.valueId,
      content: that.data.content
    }, 'POST').then(function (res) {
      if (res.errno === 0) {
        wx.showToast({
          title: '评论成功',
          complete: function(){
            wx.navigateBack({
              delta: 1
            });
          }
        })
      }
      console.log(res)
    });
  },
  submitRecord(){
    let dataparam = {}
    var that = this;
    if (!that.data.mediaUrl) {
      util.showErrorToast('请上传图片')
      return false;
    }
    if(that.data.indexT==1){
      if (!that.data.itemList.length) {
        util.showErrorToast('请输入动作组数')
        return false;
      }
    }else if(that.data.indexT==6){
      if (!that.data.desc) {
        util.showErrorToast('请输入其他说明')
        return false;
      }
    }else{
      if (!that.data.recordMinute) {
        util.showErrorToast('请输入打卡时长')
        return false;
      }
    }

    if(that.data.indexT==1){
      dataparam = {
        recordMinute: 0,
        mediaUrl: that.data.mediaUrl,
        mediaTypeId:that.data.mediaTypeId[that.data.indexMT],
        mediaTypeText:that.data.mediaTypeText[that.data.indexMT],
        recordTypeId:that.data.recordTypeId[that.data.indexT],
        recordTypeText:that.data.recordTypeText[that.data.indexT],
        action:'',
        loadWeight:0,
        group:0,
        number:0,
        desc:'',
        itemList:that.data.itemList
      }
    }else if(that.data.indexT==6){
      dataparam = {
        recordMinute: 0,
        mediaUrl: that.data.mediaUrl,
        mediaTypeId:that.data.mediaTypeId[that.data.indexMT],
        mediaTypeText:that.data.mediaTypeText[that.data.indexMT],
        recordTypeId:that.data.recordTypeId[that.data.indexT],
        recordTypeText:that.data.recordTypeText[that.data.indexT],
        action:'',
        loadWeight:0,
        group:0,
        number:0,
        desc:'',
        itemList:[]
      }
    }else{
      dataparam = {
        recordMinute: that.data.recordMinute,
        mediaUrl: that.data.mediaUrl,
        mediaTypeId:that.data.mediaTypeId[that.data.indexMT],
        mediaTypeText:that.data.mediaTypeText[that.data.indexMT],
        recordTypeId:that.data.recordTypeId[that.data.indexT],
        recordTypeText:that.data.recordTypeText[that.data.indexT],
        action:'',
        loadWeight:0,
        group:0,
        number:0,
        desc:'',
        itemList:[]
      }
    }
    
    wx.showLoading({
      title: '数据提交中',
    })
    util.request(api.RecordAdd, dataparam, 'POST').then(function (res) {
      wx.hideLoading()
      if (res.errno === 0) {
        that.setData({
          mediaUrl: '',
          indexMT:0,
          action:'',
          loadWeight:null,
          group:null,
          number:null,
          desc:'',
          itemList:[]
        })
        wx.showToast({
          title: '打卡成功',
          complete: function(){
            wx.switchTab({
              url: '../index/index'
            })
          }
        })
      }
      console.log(res)
    });
  },
  choseImage(){
    // wx.showLoading({
    //   title: '图片加载中',
    // })
    var that = this;
    util.uploadImage(api.Upload).then(function (res) {
      if (res.errno === 0) {
        console.log(res)
        that.setData({
          mediaUrl: res.data,
          indexMT:0
        })
      }
      // wx.hideLoading()
    });
  },
  choseVideo(){
    // wx.showLoading({
    //   title: '视频加载中',
    // })
    var that = this;
    util.uploadVideo(api.Upload).then(function (res) {
      if (res.errno === 0) {
        console.log(res)
        that.setData({
          mediaUrl: res.data,
          indexMT:1
        })
      }
      // wx.hideLoading()
    });
  },
  bindMediaChange(e){
    this.setData({
      indexMT: e.detail.value
    })
    if(e.detail.value==0){
      this.choseImage();
    }else{
      this.choseVideo();
    }
  },
  bindInputValue(event){
    let value = event.detail.value;
    //判断是否超过140个字符
    if (value && value.length > 140) {
      return false;
    }
    this.setData({
      content: event.detail.value,
    })
    console.log(event.detail)
  },
  bindInputRecordMinute(event){
    this.setData({
      recordMinute: event.detail.value
    })
  },
  bindInputAction(event){
    this.setData({
      action: event.detail.value
    })
  },
  bindInputLoadWeight(event){
    this.setData({
      loadWeight: event.detail.value
    })
  },
  bindInputGroup(event){
    this.setData({
      group: event.detail.value
    })
  },
  bindInputNumber(event){
    this.setData({
      number: event.detail.value
    })
  },
  bindInputDesc(event){
    this.setData({
      desc: event.detail.value
    })
  },
  bindPickerChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      indexT: e.detail.value
    })
    if(this.data.indexT==1){
      this.setData({
        recordMinute:0,
        desc:'',
        itemList:[]
      })
    }else{
      this.setData({
        action:'',
        loadWeight:null,
        group:null,
        number:null,
        recordMinute:null,
        desc:'',
        itemList:[]
      })
    }
  },
  continueAdd: function(){
    var that = this;
    if (!that.data.action) {
      util.showErrorToast('请输入动作名称')
      return false;
    }
    if (!that.data.loadWeight) {
      util.showErrorToast('请输入重量')
      return false;
    }
    if (!that.data.group) {
      util.showErrorToast('请输入组数')
      return false;
    }
    if (!that.data.number) {
      util.showErrorToast('请输入每组次数')
      return false;
    }
    var item = {'action':that.data.action,'loadWeight':that.data.loadWeight,'group':that.data.group,'number':that.data.number}
    let list = that.data.itemList
    list.push(item)
    that.setData({
      itemList:list,
      action:'',
      loadWeight:null,
      group:null,
      number:null
    })
  },
  continueCancel: function(){
    this.setData({
      action:'',
      loadWeight:null,
      group:null,
      number:null
    })
  },
  itemDelete:function(e){
    // console.log(e)
    let order = e.currentTarget.dataset.order
    let list = this.data.itemList
    let nlist = []
    for(let i=0;i<list.length;i++){
      if(i!=order){
        nlist.push(list[i])
      }
    }
    this.setData({
      itemList:nlist
    })
  },
  onReady: function () {

  },
  onShow: function () {
    
  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭

  }
})
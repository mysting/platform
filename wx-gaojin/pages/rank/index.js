var app = getApp();
var util = require('../../utils/util.js');
var api = require('../../config/api.js');
Page({
  data: {
    avatarUrl: 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIZGu5BWDoZKmcPXx0mZiaRQSdiadOKSsXe2S3Q0DkcEPMPZoqUoajETudZBlWs8icCg4W3RPIZ5F7nA/132',
    rankType: 'today',
    rankList: [],
    rankInfo:null,
    userInfo:null
  },
  onLoad: function (options) {
    this.getRankInfo();
  },
  onClose() {
    wx.navigateBack({
      delta: 1
    });
  },
  bindRank(e){
    let that = this;
    console.log(e)
    that.setData({
      rankType:e.target.dataset.type
    })
    this.getRankInfo();
  },
  getRankInfo() {
    wx.showLoading({
      title: '加载中',
    })
    let that = this;
    util.request(api.RankList, {
      rankType: that.data.rankType,
      page:1,
      size:1000
    }, 'POST').then(function (res) {
      if (res.errno === 0) {
        if(that.data.rankType=='today'){
          that.setData({
            rankList: res.data.data,
            rankInfo: res.data.todayRank,
            userInfo: res.data.user
          });
        }else if(that.data.rankType=='week'){
          that.setData({
            rankList: res.data.data,
            rankInfo: res.data.weekRank,
            userInfo: res.data.user
          });
        }else if(that.data.rankType=='total'){
          that.setData({
            rankList: res.data.data,
            rankInfo: res.data.totalRank,
            userInfo: res.data.user
          });
        }
      }
      wx.hideLoading()
      console.log(res)
    });
  },
  onReady: function () {

  },
  onShow: function () {
    // 页面显示

  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭

  }
})
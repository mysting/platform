var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');



var app = getApp();

Page({
  data: {
    content:'',
    contentLength:0,
    mobile:'',
    startDate:'',
    endDate:'',
    days:1
  },
  bindStartDateChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value);
    var that = this
    that.setData({
      startDate: e.detail.value,
      days:util.getDiffDate(e.detail.value,that.data.endDate)
    });

  },
  bindEndDateChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value);
    var that = this
    that.setData({
      endDate: e.detail.value,
      days:util.getDiffDate(that.data.startDate,e.detail.value)
    });
  },
  contentInput: function (e) {
   
    let that = this;
    this.setData({
      contentLength: e.detail.cursor,
      content: e.detail.value,
    });
    console.log(that.data.content);
  },
  sbmitLeave : function(e){
    let that = this;
    if (that.data.days == 0){
      util.showErrorToast('请选择有效的请假日期');
      return false;
    }
    if (that.data.content == '') {
      util.showErrorToast('请输入请假原因');
      return false;
    }
    wx.showLoading({
      title: '提交中...',
      mask:true,
      success: function () {

      }
    });
    util.request(api.RecordLeave, { 
      startDate: that.data.startDate, 
      endDate: that.data.endDate,
      days:that.data.days,
      desc: that.data.content
    },'POST').then(function (res) {
      wx.hideLoading();
      if (res.errno === 0) {
        console.log(res.data);
        
        wx.showToast({
          title: '请假成功',
          complete: function(){
            wx.switchTab({
              url: '../../index/index'
            })
          }
        })
      } else {
        util.showErrorToast(res.errmsg);
      }
      
    });
  },
  onLoad: function (options) {
    this.setData({
      startDate:util.getFormatDate(),
      endDate:util.getFormatDate()
    })

    this.setData({
      userInfo:app.globalData.userInfo
    })

    if(app.globalData.token==''&& app.globalData.userInfo==null){
      //需要登录后才可以操作
      wx.showModal({
        title: '',
        content: '请先登录',
        success: function (res){
          if (res.confirm) {
            wx.switchTab({
              url: '/pages/ucenter/index/index'
            });
          }else{
            wx.switchTab({
              url: '/pages/index/index'
            });
          }
        }
      });
    }else if(app.globalData.user && app.globalData.user.status<1){
      //需要登录后才可以操作
      wx.showModal({
        title: '',
        content: '请先补充个人信息',
        success: function (res){
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/auth/loginInfo/index'
            });
          }else{
            wx.switchTab({
              url: '/pages/index/index'
            });
          }
        }
      });
    }

  },
  onReady: function () {

  },
  onShow: function () {

  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭
  }
})
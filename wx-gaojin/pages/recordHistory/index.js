const util = require('../../utils/util.js');
const api = require('../../config/api.js');
const user = require('../../services/user.js');

//获取应用实例
const app = getApp()
Page({
  data: {
    screenHeight: 0,
    userInfo: null,
    user:null,
    totalRank:null,
    weekRank:null,
    dataList:[],
    totalCount:0,
    pageSize:10,
    totalPage:0,
    currPage:1,
    requestLoading:false,
    requestLoadingComplete:false,
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    commentDesc:"",
    defaultUrl:"../../static/images/user.png",
    focusFlag:false,
    showInput:false,
    currentRecord:null,
    readyToSend:'',
    inputValue: '弹幕',
    src: '',
  },
  onShareAppMessage: function () {
    return {
      title: '高金俱乐部',
      desc: '高金俱乐部',
      path: '/pages/index/index'
    }
  },
  getIndexData: function () {
    wx.showLoading({
      title: '努力加载中',
    });
    let that = this;
    util.request(api.RankHistoryList).then(function (res) {
      console.log(res);
      if (res.errno === 0) {
        // var formatData = res.data.data.data;
        // for(var i=0;i<formatData.length;i++){
        //   formatData[i].formatTime = util.getDateDiff(util.getDateTimeStamp(formatData[i].createTime))
        // }
        let screenHeight = that.data.screenHeight;
        let screenOrderNum = parseInt(screenHeight / 28);
        console.log(screenOrderNum, '满屏最多能放几个卡片')
        // 9 >= 1, 2, 3 知道 currentPage 大于 rap数据总页码
        if (res.data.data.totalPage >= that.data.currPage && (res.data.data.totalCount > screenOrderNum)) {  
          that.setData({
            userInfo:res.data.loginUser,
            totalRank:res.data.totalRank,
            weekRank:res.data.weekRank,
            requestLoading: true,
            dataList:that.data.dataList.concat(res.data.data.list),
            totalCount:res.data.data.totalCount,
            pageSize:res.data.data.pageSize,
            totalPage:res.data.data.totalPage,
            currPage:res.data.data.currPage
          }); 
        } else if (res.data.data.totalPage >= that.data.currPage && (res.data.data.totalCount <= screenOrderNum)) {

            that.setData({
              userInfo:res.data.loginUser,
              totalRank:res.data.totalRank,
              weekRank:res.data.weekRank,
              requestLoadingComplete: false,
              requestLoading: false,
              dataList:that.data.dataList.concat(res.data.data.list),
              totalCount:res.data.data.totalCount,
              pageSize:res.data.data.pageSize,
              totalPage:res.data.data.totalPage,
              currPage:res.data.data.currPage
            }); 
        } else {
            that.setData({
                requestLoadingComplete: true,
                requestLoading: false
            })
            that.setData({
              requestLoadingComplete: true,
              requestLoading: false
            }); 
        }
        wx.hideLoading();
        wx.stopPullDownRefresh();
      }
    });
  },
  /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
  onPullDownRefresh: function () {
    console.log("page下拉动作")
    
    this.setData({
        currPage: 1,
        dataList: [],
        requestLoading: false,
        requestLoadingComplete: false
    });
    this.getIndexData();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
      console.log("page上拉触底")
      let that = this;
      if (that.data.requestLoading && !that.data.requestLoadingComplete) {
        that.setData({
          currPage: that.data.currPage + 1,  //每次触发上拉事件，把requestPageNum+1
        })
        that.getIndexData();
      }
  },
  onLoad: function (options) {
    // 获取系统高度，判断正在加载中是否显示, 每个卡片的高度是300rpx;
    let that = this;
    wx.getSystemInfo({
        success: function (res) {
            console.log(res.windowHeight, 'screenHeight')
            that.setData({
                screenHeight: res.windowHeight
            })
        },
    })
    this.getIndexData();
  },
  onReady: function () {
    // 页面渲染完成
    this.videoContext = wx.createVideoContext('myVideo')
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../record/index'
    })
  }
})

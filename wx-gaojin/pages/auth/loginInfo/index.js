var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
var app = getApp();
Page({
  data: {
    username: '',
    classroom:'',
    mobile:'',
    loginErrorCount: 0
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    // 页面渲染完成
  },
  onReady: function () {

  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  bindCheckMobile: function (mobile) {
    if(!mobile){
        wx.showModal({
          title: '错误',
          content: '请输入手机号码'
        });
        return false
    }
    if (!mobile.match(/^1[3-9][0-9]\d{8}$/)) {
        wx.showModal({
          title: '错误',
          content: '手机号格式不正确，仅支持国内手机号码'
        });
        return false
    }
    return true
  },
  startRegister: function () {
    var that = this;
    if (that.data.username.length < 2) {
      wx.showModal({
        title: '错误信息',
        content: '姓名不得少于2位',
        showCancel: false
      });
      return false;
    }

    if (that.data.classroom.length < 2) {
      wx.showModal({
        title: '错误信息',
        content: '班级不得少于2位',
        showCancel: false
      });
      return false;
    }
    
    if (!this.bindCheckMobile(this.data.mobile)) {
      return
    }

    wx.showLoading({
      title: '提交中...',
      mask:true,
      success: function () {

      }
    });

    util.request(api.RecordLoginInfo, {
      realname: that.data.username, 
      classroom: that.data.classroom,
      mobile:that.data.mobile
    },'POST').then(function (res) {
      wx.hideLoading();
      if (res.errno === 0) {
        console.log(res.data);
        app.globalData.user.status = 1;
        wx.setStorageSync('user',app.globalData.user);
        wx.showToast({
          title: '保存成功',
          complete: function(){
            wx.switchTab({
              url: '../../index/index'
            })
          }
        })
      } else {
        util.showErrorToast(res.errmsg);
      }
    });
  },
  bindUsernameInput: function (e) {
    this.setData({
      username: e.detail.value
    });
  },
  bindClassroomInput: function (e) {
    this.setData({
      classroom: e.detail.value
    });
  },
  bindMobileInput: function (e) {
    this.setData({
      mobile: e.detail.value
    });
  },
  clearInput: function (e) {
    switch (e.currentTarget.id) {
      case 'clear-username':
        this.setData({
          username: ''
        });
        break;
      case 'clear-classroom':
        this.setData({
          classroom: ''
        });
        break;
      case 'clear-mobile':
        this.setData({
          mobile: ''
        });
        break;
    }
  }
})
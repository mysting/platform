const util = require('../../utils/util.js');
const api = require('../../config/api.js');
const user = require('../../services/user.js');

//获取应用实例
const app = getApp()
Page({
  data: {
    isShowAhturoizeWarning:true,
    userInfo: {},
    user:null,
    totalRank:null,
    weekRank:null,
    dataList:[],
    totalCount:0,
    pageSize:10,
    totalPage:0,
    currPage:1,
    requestLoading:false,
    requestLoadingComplete:false,
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    commentDesc:'',
    defaultUrl:"../../static/images/user.png",
    focusFlag:false,
    showInput:false,
    currentRecord:null,
    readyToSend:'',
    inputValue: '弹幕',
    noticebar: {
      color: '#ff7b2f',
      backgroundColor: '#fcffed',
      text: '俱乐部通知：本周五线下足球赛开始报名！',
      leftIcon: '../../static/images/notice.png',
      mode: 'closeable',
      scrollable: true,
      speed: 10
    },
    src: '',
    banner: [],
    dataType:'all'
  },
  onShareAppMessage: function () {
    return {
      title: '高金俱乐部',
      desc: '高金俱乐部',
      path: '/pages/index/index'
    }
  },
  onPullDownRefresh: function () {
    console.log("page下拉动作")
    // this.setData({
    //     currPage: 1,
    //     dataList: [],
    //     requestLoading: false,
    //     requestLoadingComplete: false
    // });
    wx.showLoading({
      title: '努力加载中',
    });
    this.getIndexData();
    wx.hideLoading();
  },
  getIndexData: function () {
    var dataUrl = api.RecordListAll;
    if(this.data.dataType=='index'){
      dataUrl = api.RecordListIndex;
    }
    let that = this;
    // let id = that.data.user.userId?that.data.user.userId:'';
    util.request(dataUrl,{
      page:1,
      size:1000
    }).then(function (res) {
      console.log(res);
      if (res.errno === 0) {
        var formatData = res.data.data.data;
        for(var i=0;i<formatData.length;i++){
          formatData[i].formatTime = util.getDateDiff(util.getDateTimeStamp(formatData[i].createTime))
        }
        that.setData({
          // user:res.data.loginUser,
          banner:res.data.banner,
          totalRank:res.data.totalRank,
          weekRank:res.data.weekRank?res.data.weekRank:null,
          dataList:formatData
        });
      }
    });
  },
  onLoad: function (options) {
    // app.getUserInfo();
    // this.getIndexData();
    var that = this;
    //获取用户的登录信息
    user.checkLogin().then(res => {
      console.log('app login')

      app.globalData.userInfo = wx.getStorageSync('userInfo');
      app.globalData.token = wx.getStorageSync('token');
      app.globalData.user = wx.getStorageSync('user');
      that.setData({
        userInfo:app.globalData.userInfo,
        user:app.globalData.user
      });
    }).catch(() => {
    });

    //this.getIndexData();
    if(app.globalData.token=='' && app.globalData.userInfo==null){
      that.setData({
        dataType:'all'
      });
      that.getIndexData()
      //需要登录后才可以操作
      wx.showModal({
        title: '',
        content: '请先登录',
        success: function (res){
          if (res.confirm) {
            wx.switchTab({
              url: '/pages/ucenter/index/index'
            });
          }
        }
      })
    }else{
      that.setData({
        userInfo:app.globalData.userInfo,
        user:app.globalData.user,
        dataType:'index'
      });
      this.getIndexData();
    }
  },
  onReady: function () {
    // 页面渲染完成
    this.videoContext = wx.createVideoContext('myVideo')
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  getUserInfo: function(e) {
    console.log(e)
    var that = this;
    // app.globalData.userInfo = e.detail.userInfo;
    if(app.globalData.token=='' && app.globalData.userInfo==null){
      // that.setData({
      //   userInfo: e.detail.userInfo,
      //   hasUserInfo: true
      // })
      //用户按了允许授权按钮
      user.loginByWeixin(e.detail).then(res => {
        this.setData({
            userInfo: res.data.userInfo
        });
        app.globalData.userInfo = res.data.userInfo;
        app.globalData.token = res.data.token;
        this.getIndexData();
      }).catch((err) => {
          console.log(err)
      });
    }
  },
  doLeave:function(){
    if(this.data.user==null){
      wx.showModal({
        title: '',
        content: '请先登录',
        success: function (res){
          if (res.confirm) {
            wx.switchTab({
              url: '/pages/ucenter/index/index'
            });
          }
        }
      })
    }else if(this.data.user.status < 1){
      wx.showModal({
        title: '',
        content: '请先补充个人信息',
        success: function (res){
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/auth/loginInfo/index'
            });
          }
        }
      })
    }else if(this.data.user.status >= 1){
      wx.navigateTo({
        url: '../ucenter/leave/index'
      })
    }
    
  },
  doRecord:function(){
    if(this.data.user==null){
      wx.showModal({
        title: '',
        content: '请先登录',
        success: function (res){
          if (res.confirm) {
            wx.switchTab({
              url: '/pages/ucenter/index/index'
            });
          }
        }
      })
    }else if(this.data.user.status < 1){
      wx.showModal({
        title: '',
        content: '请先补充个人信息',
        success: function (res){
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/auth/loginInfo/index'
            });
          }
        }
      })
    }else if(this.data.user.status >= 1){
      wx.navigateTo({
        url: '../record/index'
      })
    }
  },
  //事件处理函数
  bindViewTap: function() {
    // wx.navigateTo({
    //   url: '../record/index'
    // })
  },
  bindInputBlur: function(e) {
    this.inputValue = e.detail.value
  },
  // bindButtonTap: function() {
  //   var that = this
  //   wx.chooseVideo({
  //     sourceType: ['album', 'camera'],
  //     maxDuration: 60,
  //     camera: ['front','back'],
  //     success: function(res) {
  //       that.setData({
  //         src: res.tempFilePath
  //       })
  //     }
  //   })
  // },
  bindSendDanmu: function () {
    this.videoContext.sendDanmu({
      text: this.inputValue,
      color: util.getRandomColor()
    })
  },
  bindRecordPraise:function(e){
    let that = this;
    if(that.data.user==null){
      util.showErrorToast("请先登录")
      return false
    }
    var current_record = e.target.dataset.record;
    
    util.request(api.RecordPraise, {
      recordId: current_record.id,
      userId: that.data.user.userId,
      userName: that.data.user.nickname
    }, 'POST').then(function (res) {
      if (res.errno === 0) {
        //do something
        that.getIndexData();
      }else{
        util.showErrorToast(res.errmsg)
      }
      console.log(res)
    });
  },
  bindRecordInput:function(e){
    if(this.data.user==null){
      util.showErrorToast("请先登录")
      return false
    }
    this.setData({
      focusFlag:false,
      showInput:true,
      currentRecord:e.target.dataset.record,
      readyToSend:'',
      commentDesc:''
    })
  },
  bindRecordComment: function(e) {
    let that = this;
    var current_record = e.target.dataset.record;
    if (!that.data.commentDesc) {
      util.showErrorToast('请填写评论')
      return false;
    }
    util.request(api.RecordComment, {
      recordId: current_record.id,
      fromId:current_record.userId,
      fromName:current_record.userName,
      userId: that.data.user.userId,
      userName: that.data.user.nickname,
      commentDesc:that.data.commentDesc
    }, 'POST').then(function (res) {
      if (res.errno === 0) {
        that.getIndexData();
        wx.showToast({
          title: '评论成功',
          complete: function(){
            //do something
          }
        })
      }
      console.log(res)
    });
  },
  bindCommentItemDelete: function(e){
    var current_id = e.target.dataset.id;
    util.request(api.RecordCommentDelete, {
      id: current_id
    }, 'POST').then(function (res) {
      if (res.errno === 0) {
        that.getIndexData();
        wx.showToast({
          title: '删除成功',
          complete: function(){
            //do something
          }
        })
      }else{
        util.showErrorToast(res.errmsg)
      }
      console.log(res)
    })
  },
  bindconfirmInput: function(e){
    console.log(e)
    this.setData({
      commentDesc: e.detail.value || ''
    })
  },
  myinputSend (evt) {
    var that = this;
    if (!that.data.commentDesc) {
      util.showErrorToast('请填写评论')
      return false;
    }
    util.request(api.RecordComment, {
      recordId: that.data.currentRecord.id,
      fromId:that.data.currentRecord.userId,
      fromName:that.data.currentRecord.userName,
      userId: that.data.user.userId,
      userName: that.data.user.nickname,
      commentDesc:that.data.commentDesc
    }, 'POST').then(function (res) {
      if (res.errno === 0) {
        wx.showToast({
          title: '评论成功',
          complete: function(){
            //do something
            that.setData({
              readyToSend:'',
              currentRecord: null,
              showInput:false
            });
            that.getIndexData();
          }
        })
      }
      console.log(res)
    });
  },
  myinputInput (evt) {
    console.log(evt.detail.value)
    this.setData({
      commentDesc:evt.detail.value
    })
  },
  myinputFocus (evt) {
    this.setData({
      focusFlag:true
    })
  },
  myinputBlur (evt) {
    this.setData({
      focusFlag:false,
      showInput:false
      // currentRecord:null,
      // commentDesc:''
    })
  },
  myinputClear () {
    this.setData({
      commentDesc:''
    })
  }
})

/*
Navicat MySQL Data Transfer

Source Server         : 120.79.130.222
Source Server Version : 50617
Source Host           : 120.79.130.222:13306
Source Database       : platform

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2018-07-05 20:18:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gaojin_record`
-- ----------------------------
DROP TABLE IF EXISTS `gaojin_record`;
CREATE TABLE `gaojin_record` (
  `id` bigint(32) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(8)  DEFAULT '0' COMMENT '用户ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户姓名',
  `record_type_id` tinyint(1) DEFAULT NULL COMMENT '打卡类别',
  `record_type_text` varchar(60)  DEFAULT '' COMMENT '打卡类别名称',
  `media_type_id` tinyint(1) DEFAULT NULL COMMENT '媒体类别',
  `media_type_text` varchar(60) DEFAULT '' COMMENT '媒体类别名',
  `record_minute` mediumint(4) DEFAULT '0' COMMENT '打卡分钟',
  `status_id` tinyint(1) DEFAULT '1' COMMENT '状态值1有效 0无效',
  `status_text` varchar(60) DEFAULT '有效' COMMENT '状态值名',
  `media_url` text COMMENT '媒体url',
  `latitude`  varchar(60) DEFAULT '' COMMENT '纬度',
  `longitude`  varchar(60) DEFAULT '' COMMENT '经度',
  `address`  varchar(60) DEFAULT '' COMMENT '地址',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `record_time` datetime DEFAULT NULL COMMENT '打卡时间',
  `confirm_time` datetime DEFAULT NULL COMMENT '审核时间',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='打卡表';


-- ----------------------------
-- Table structure for `gaojin_record_item`
-- ----------------------------
DROP TABLE IF EXISTS `gaojin_record_item`;
CREATE TABLE `gaojin_record_item` (
  `id` bigint(32) unsigned NOT NULL AUTO_INCREMENT,
  `record_id` bigint(32) DEFAULT '0' COMMENT '用户ID',
  `item_id` tinyint(1) DEFAULT NULL COMMENT '子项',
  `item_type_text` varchar(60)  DEFAULT '' COMMENT '子项名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `item_desc` varchar(500)  DEFAULT '' COMMENT '描述',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='打卡子项表';



-- ----------------------------
-- Table structure for `gaojin_today_rank`
-- ----------------------------
DROP TABLE IF EXISTS `gaojin_today_rank`;
CREATE TABLE `gaojin_today_rank` (
  `id` bigint(32) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(32) DEFAULT '0' COMMENT '用户ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户姓名',
  `today_count` mediumint(4) DEFAULT '0' COMMENT '',
  `today_minute` mediumint(4) DEFAULT '0' COMMENT '',
  `today_time` datetime DEFAULT NULL COMMENT '创建时间',
   PRIMARY KEY (`id`),
   KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='今日排行表';


-- ----------------------------
-- Table structure for `gaojin_week_rank`
-- ----------------------------
DROP TABLE IF EXISTS `gaojin_week_rank`;
CREATE TABLE `gaojin_week_rank` (
  `id` bigint(32) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(8) DEFAULT '0' COMMENT '用户ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户姓名',
  `week_count` mediumint(4) DEFAULT '0' COMMENT '',
  `week_minute` mediumint(4) DEFAULT '0' COMMENT '',
  `start_time` datetime DEFAULT NULL COMMENT '本周开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '本周结束时间',
   PRIMARY KEY (`id`),
   KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='本周排行表';

-- ----------------------------
-- Table structure for `gaojin_total_rank`
-- ----------------------------
DROP TABLE IF EXISTS `gaojin_total_rank`;
CREATE TABLE `gaojin_total_rank` (
  `id` bigint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(32) DEFAULT '0' COMMENT '用户ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户姓名',
  `total_count` mediumint(4) DEFAULT '0' COMMENT '',
  `total_minute` mediumint(8) DEFAULT '0' COMMENT '',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
   PRIMARY KEY (`id`),
   KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='总排行表';

-- ----------------------------
-- Table structure for `gaojin_record_praise`
-- ----------------------------
DROP TABLE IF EXISTS `gaojin_record_praise`;
CREATE TABLE `gaojin_record_praise` (
  `id` bigint(32) unsigned NOT NULL AUTO_INCREMENT,
  `record_id` bigint(32) NOT NULL,
  `user_id` bigint(32) DEFAULT '0' COMMENT '用户ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户姓名',
  `status_id` tinyint(1) DEFAULT '1' COMMENT '状态值1有效 0无效',
  `status_text` varchar(60) DEFAULT '有效' COMMENT '状态值名',
  `praise_time` datetime DEFAULT NULL COMMENT '点赞时间',
   PRIMARY KEY (`id`),
   KEY `record_id` (`record_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='打卡点赞表';



-- ----------------------------
-- Table structure for `gaojin_record_comment`
-- ----------------------------
DROP TABLE IF EXISTS `gaojin_record_comment`;
CREATE TABLE `gaojin_record_comment` (
  `id` bigint(32) unsigned NOT NULL AUTO_INCREMENT,
  `record_id` bigint(32) NOT NULL,
  `user_id` bigint(32) DEFAULT '0' COMMENT '评论用户ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '评论用户姓名',
  `from_id` mediumint(8) DEFAULT NULL COMMENT '用户ID',
  `from_name` varchar(50) DEFAULT '' COMMENT '用户姓名',
  `comment_desc` varchar(500)  DEFAULT '' COMMENT '描述',
  `status_id` tinyint(1) DEFAULT '1' COMMENT '状态值1有效 0无效',
  `status_text` varchar(60) DEFAULT '有效' COMMENT '状态值名',
  `comment_time` datetime DEFAULT NULL COMMENT '评论时间',
   PRIMARY KEY (`id`),
   KEY `record_id` (`record_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='打卡评论表';